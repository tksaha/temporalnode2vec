#!/bin/bash

#valid, test
export TEST_MODE=VALID
# avg, wgtL1, wgtL2, add
export COMPOSITION=hadamard
# link prediction or forecast (PRED, FORE)
export TASK=PRED
# how many times to repeat
export REPEAT=3

# random seed for multiple pass
export RND_SEED=1000

export CUDA_VISIBLE_DEVICES=""
export ROOTDIR=/home/tanay/Documents/temporalnode2vec
export DATADIR=$ROOTDIR/Data/
export ENRONFILE=$DATADIR/Enron/EnronDirectedWithCc_7Days.mat
export ENRONSTTIME=147
export ENRONENDTIME=157
export DBLP2STTIME=0
export DBLP2ENDTIME=9
export DBLP3STTIME=0
export DBLP3ENDTIME=9
export NIPSSTTIME=0
export NIPSENDTIME=16
export FB2STTIME=0
export FB2ENDTIME=8
export ENRONNODE2VECDIR=$DATADIR

export LINEEXEFILE=$ROOTDIR/temporalnode2vec/lineLinux/line
export NORMALIZEEXEFILE=$ROOTDIR/temporalnode2vec/lineLinux/normalize
export CONCATEXE=$ROOTDIR/temporalnode2vec/lineLinux/concatenate
export SMSAFILE=$DATADIR/SMS-A.txt
export COLLEGEFILE=$DATADIR/CollegeMsg.txt
export EMAILEUFILE=$DATADIR/email-Eu-core.txt 
export INFECTFILE=$DATADIR/sociopatterns-infectious/out.sociopatterns-infectious
export FBFILE=$DATADIR/facebook-wosn-links/out.facebook-wosn-links
export HEPPHFILE=$DATADIR/ca-cit-HepPh/out.ca-cit-HepPh
export DBLPFILE=$DATADIR/dblp_coauthor/out.dblp_coauthor
export DBLP2FILE=$DATADIR/DBLP2/citation2Filtered.mat
export DBLP3FILE=$DATADIR/DBLP3/citation3Filtered.mat
export NIPSFILE=$DATADIR/NIPS/NipsAdj_17Years.mat
export FB2FILE=$DATADIR/Facebook/FacebookFilteredAdj_90Days_6ActiveTimes_30OutInDeg.mat
export YTUBEFILE=$DATADIR/youtube-u-growth/out.youtube-u-growth
export NUM_WALK=10
export WALK_LEN=20
export DOC2VECEXECDIR=$ROOTDIR/temporalnode2vec/word2vec/word2vec
export RETROFITONEEXE=$ROOTDIR/temporalnode2vec/word2vec/retrofit_word2vec_one
export BCGDEXEFILE=$ROOTDIR/temporalnode2vec/bcgd/BCGDEmbed
