Data construction details:

Citation2

1.     Collected from: https://aminer.org/billboard/citation

2.     1,397,240 papers and >3,021,489 citation relationships (2010-09-13).

3.     Authors with latest paper on or after year 2010 and at least 11 years of activity: 2389

4.     2010 has very few edges 845 (where 2009 has 2500 avg is around 2100)

5.     time: 2000-2009: 10 years

6.     edges: 1976         2440   2406   2512   2574   2804   2838   2986   2964   3600

7.     Nodes with at least 2 edges in at least 7 time stamp: 315

8.     Edges 330            460     466     520     506     594     540     574     498     616