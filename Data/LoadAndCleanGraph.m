function adj = LoadAndCleanGraph(input)
    %load input dataset
    load(input)

    %delete everything except needed variables
    clearvars -except adj

    %get number of nodes (r,c) and number of time stamps time
    [r,c,time]=size(adj);

    %preprocess data: eliminate self loop and make graph undirected
    for timeIter=1:time
        t=adj(:,:,timeIter);
        for u=1:r
            for v=1:c
                if u==v
                    t(u,v)=0; %eliminate self loop
                    continue
                end
                if t(u,v)>0 %makes the graph undirected
                    t(u,v)=1;
                    t(v,u)=1;
                end
            end
        end
        adj(:,:,timeIter)=t;
        NoEdges=sum(sum(t));
        %fprintf('snap: %d #edges: %d\n', timeIter, NoEdges/2)
    end
end
