Data construction details:

Citation3

1.     Collected from: https://aminer.org/billboard/citation

2.     1,632,442 papers and >2,327,450 citation relationships (2010-10-22).

3.     Authors with latest paper on or after year 2010 and at least 11 years of activity: 7461

4.     2010:6890 edges, 2011:38 edges

5.     time: 2000-2009: 10 years

6.     edges: 12694      14000            15190            16314            16914            18244            17278            17056            17486      15982

7.     Nodes with at least 4 edges in at least 7 time stamp: 653

8.     Edges: 1358        1552   1928   1972   1946   2130   1780   1796   1866   1832