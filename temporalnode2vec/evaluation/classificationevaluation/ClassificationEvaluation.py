#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import math
import pickle
import importlib
import numpy as np
import pandas as pd
import sklearn.metrics as mt
from sklearn import linear_model
from abc import ABCMeta, abstractmethod
from logManager.Logger import Logger 
from sklearn.dummy import DummyClassifier


class ClassificationEvaluation:
    """
    ClassificationEvaluation Base
    """
    __metaclass__ = ABCMeta

    """
    true_values = A collection of true values
    predicted_values = A collection of predicted values
    class_labels = A dictionary of class ids/keys and names/values. 
    {0: "People", 1: "Topic", 2: "Company", ...}
    """
    def __init__(self, *args, **kwargs):
        self.true_values = []
        self.predicted_values = []
        self.predicted_scores_pos = []
        self.class_keys = {}
        self.class_names = []
        self.trainTestFolder = os.environ['DATADIR']
        self.composition_method = os.environ['COMPOSITION']
        self.isMerged = kwargs['isMerged']

        # Edge Feature Generator 
        self.module_dict = {"avg": "edgeFeatureGenerator.AverageEdgeFeatGen",\
         "hadamard": "edgeFeatureGenerator.HadamardFeatGen",\
        "wgtL1": "edgeFeatureGenerator.WeightedL1Gen",\
        "wgtL2": "edgeFeatureGenerator.WeightedL2Gen",\
        "add": "edgeFeatureGenerator.AdditionFeatGen"}

        self.class_dict = {"avg":"AverageEdgeFeatGen", "hadamard": "HadamardFeatGen",\
        "wgtL1":"WeightedL1FeatGen", "wgtL2": "WeightedL2FeatGen", "add": "AdditionFeatGen"} 


    """
    Private Methods for evaluation 
    """
    def _getConfusionMatrix(self):
        return mt.confusion_matrix(self.true_values, self.predicted_values, labels = self.class_keys)
        
    def _getCohenKappaScore(self):
        return mt.cohen_kappa_score(self.true_values, self.predicted_values, labels = self.class_keys)
        
    def _getClassificationReport(self):
        return mt.classification_report(self.true_values, self.predicted_values,\
             labels = self.class_keys, target_names = self.class_names, digits=4)
    
    def _getAccuracyScore(self):
        return mt.accuracy_score(self.true_values, self.predicted_values)

    def _getAUCScore(self):
        fpr, tpr, thresholds = mt.roc_curve(self.true_values,\
         self.predicted_scores_pos,\
          pos_label='pos')
        return  mt.auc(fpr, tpr)

    def _getBinaryLabelValues(self):
        binary_label_values = []
        for labels in self.true_values:
            if labels == 'pos':
                binary_label_values.append(1)
            else:
                binary_label_values.append(0)
        return binary_label_values

    def _getAUPRC(self):
        return mt.average_precision_score(self._getBinaryLabelValues(),\
             self.predicted_scores_pos)
        
    def __dcg_score(self, y_true, y_score, k=10, gains="exponential"):
        order = np.argsort(y_score)[::-1]
        y_true = np.take(y_true, order[:k])

        if  gains == "exponential":
            gains = 2.0 ** y_true - 1
        elif gains == "linear":
            gains = y_true
        else:
            raise ValueError("Invalid gains option.")

        discounts = np.log2(np.arange(len(y_true)) + 2) 
        return np.sum(gains / discounts)

    def __ndcg_score(self, y_true, y_score, k=10, gains="exponential"):
        best = self.__dcg_score(y_true, y_true, k, gains)
        actual = self.__dcg_score(y_true, y_score, k, gains)
        return float(actual) / best

    def _getNDCGScore(self):
        ndcg = self.__ndcg_score(self._getBinaryLabelValues(),\
                     self.predicted_scores_pos, k=50,gains="exponential")
        #print (self.predicted_scores_pos)
        return ndcg

    def _writeClassificationData(self, id_label_filename, fileToWrite):
        """
        """
        module = self.module_dict[self.composition_method]
        klass = self.class_dict[self.composition_method]
        Klass = getattr(importlib.import_module(module), klass)
        composite_klass = Klass(vecDict=self.v2vec) 

        line_no = 1

        for line in open(id_label_filename):
            line_elems = line.strip().split(",")
            id1 = int(line_elems[0])
            id2 = int(line_elems[1])
            topic = line_elems[2]

            vec = composite_klass.getFeatData(id1, id2)
            vec_str = ','.join(str("%.5f"%x) for x in vec)
            fileToWrite.write("%s,%s,%s%s"%(line_no,vec_str,topic, os.linesep))
            line_no = line_no + 1




    def _writeClassificationReport(self, evaluationResultFile, latReprName):

        accuracy = self._getAccuracyScore()
        auc = self._getAUCScore()
        auprc = self._getAUPRC()
        ndcg = self._getNDCGScore()

        evaluationResultFile.write("%s%s%s%s" %("######Classification Report",\
                    "(%s ==> %s)######\n"%(latReprName, self.composition_method), \
                    self._getClassificationReport(), "\n\n"))
        Logger.logr.info ("%s%s"%(os.linesep, self._getClassificationReport()))
        evaluationResultFile.write("%s%s%s" %("######Accuracy Score######\n", \
                    accuracy, "\n\n"))
        Logger.logr.info(accuracy)
        evaluationResultFile.write("%s%s%s" %("######Confusion Matrix######\n", \
                    self._getConfusionMatrix(), "\n\n"))
        Logger.logr.info("%s%s"%(os.linesep, self._getConfusionMatrix()))
        evaluationResultFile.write("%s%s%s" %("######Cohen's Kappa######\n", \
                    self._getCohenKappaScore(), "\n\n"))
        Logger.logr.info(self._getCohenKappaScore())
        evaluationResultFile.write("%s%s%s" %("######AUC######\n", \
                   auc, "\n\n"))  
        Logger.logr.info(auc) 
        evaluationResultFile.write("%s%s%s" %("######AUPRC######\n", \
                   auprc, "\n\n"))
        Logger.logr.info(auprc)
        evaluationResultFile.write("%s%s%s" %("######NDCG@50######\n", \
                   ndcg, "\n\n"))
        Logger.logr.info(ndcg)
        Logger.logr.info("Evaluation with Logistic regression Completed.")
        return (accuracy, auc, auprc, ndcg)

    def _getXY(self, data):
        """
        This function assumes that the data (pandas DF) has id in the 
        first column, label in the last column and features 
        in the middle. It returns features as X and label as Y.
        """
        X = data[data.columns[1: data.shape[1]-1 ]]
        Y = data[data.shape[1]-1]
        return (X, Y)

    
    
    """
    """
    @abstractmethod
    def generateData(self, snapID, latReprName, composition_method):

        pass 


    def runClassificationTask(self, snapID, latReprName):
        """
        This function uses the generated train and test 
        files to build a logistic regression model using 
        scikit-learn. It will save the results 
        into files.
        """

        #Based on the test mode choose test or validation set for testing
        
        train = pd.read_csv("%s/%s/%strain_%i_%s.csv"%(self.trainTestFolder,\
             self.dataset_name, latReprName, snapID, self.composition_method), header=None)

        if os.environ['TEST_MODE']=='TEST':
            test = pd.read_csv("%s/%s/%stest_%i_%s.csv"%(self.trainTestFolder,\
             self.dataset_name, latReprName, snapID, self.composition_method), header=None)
        else:
            test = pd.read_csv("%s/%s/%svalid_%i_%s.csv"%(self.trainTestFolder,\
             self.dataset_name, latReprName, snapID, self.composition_method), header=None)
                                    
        train_X, train_Y = self._getXY(train)
        test_X, test_Y = self._getXY(test)

##################### Logistic Regression ###########################           
        logistic = linear_model.LogisticRegression()
        logit = logistic.fit(train_X, train_Y)
            
        result = pd.DataFrame()
        result['predicted_values'] = logit.predict(test_X)

        result['predicted_scores_pos'] = logit.predict_proba(test_X)\
                    [:,list(logit.classes_).index('pos')]
        result['true_values'] = test_Y


        #result.to_csv("%s/%sresult_%i.csv"%(self.trainTestFolder,\
        #   latReprName, snapID), index=False)
            
        labels = set(result['true_values'])
        class_labels = {}
        for i, label in enumerate(labels):
            class_labels[label] = label
            
        self.true_values =  result['true_values']
        self.predicted_values = result['predicted_values']

        self.predicted_scores_pos = result['predicted_scores_pos']
        self.class_keys = sorted(class_labels)
        self.class_names = [class_labels[key] for key in self.class_keys]
        evaluationResultFile = open("%s/%s/%seval_%i_%s.txt"%(self.trainTestFolder,\
                self.dataset_name, latReprName, snapID, self.composition_method), "w")
        
        return self._writeClassificationReport(evaluationResultFile, latReprName)