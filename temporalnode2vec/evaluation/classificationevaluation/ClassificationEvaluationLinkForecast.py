#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import math
import pickle
import importlib
import numpy as np
import pandas as pd
import sklearn.metrics as mt
from sklearn import linear_model
from abc import ABCMeta, abstractmethod
from logManager.Logger import Logger 
from sklearn.dummy import DummyClassifier
from evaluation.classificationevaluation.ClassificationEvaluation import ClassificationEvaluation



class ClassificationEvaluationLinkForecast(ClassificationEvaluation):
    def __init__(self, *args, **kwargs):
        ClassificationEvaluation.__init__(self, *args, **kwargs)

    def generateData(self, snapID, latReprName, composition_method):
        Logger.logr.info ("Evaluating for Link Forecasting")

        dataset_name = os.environ['DATASET']
        self.composition_method = composition_method
        self.dataset_name = dataset_name


        trainFileToWrite = open("%s/%s/%strain_%i_%s.csv"%(self.trainTestFolder,\
             self.dataset_name, latReprName, snapID, self.composition_method), "w")
        
        file_to_load = ""

        if "linear" in latReprName:
            file_to_load = open("%s/%s/%s_repr_%i.p"%(self.trainTestFolder,\
                 dataset_name, latReprName, snapID),"rb")
            Logger.logr.info ("Working for linear projection~~~~~~~")
        else:
            file_to_load = open("%s/%s/%s_repr_%i.p"%(self.trainTestFolder,\
                 dataset_name, latReprName, snapID-1),"rb")
            


        if self.isMerged:
            id_label_filename = "%s/%s/nidlabel_%i_merged.txt"%(self.trainTestFolder,\
                 dataset_name, snapID-1)
        else:
            id_label_filename = "%s/%s/nidlabel_%i.txt"%(self.trainTestFolder,\
                 dataset_name, snapID-1)

        self.v2vec = pickle.load(file_to_load)
        self._writeClassificationData(id_label_filename, trainFileToWrite)



        id_label_filename = ""


        if os.environ['TEST_MODE'] == 'TEST':
            testFileToWrite = open("%s/%s/%stest_%i_%s.csv"%(self.trainTestFolder,\
                self.dataset_name, latReprName, snapID, self.composition_method), "w")
            id_label_filename = "%s/%s/nidlabel_%i.txt"%(self.trainTestFolder,\
                 dataset_name, snapID)
        else:
            testFileToWrite = open("%s/%s/%svalid_%i_%s.csv"%(self.trainTestFolder,\
                self.dataset_name, latReprName, snapID, self.composition_method), "w")

            id_label_filename = "%s/%s/nidlabel_%i_valid.txt"%(self.trainTestFolder,\
                 dataset_name, snapID)

        self._writeClassificationData(id_label_filename, testFileToWrite)
        
        trainFileToWrite.flush()
        testFileToWrite.flush()
        trainFileToWrite.close()
        testFileToWrite.close()