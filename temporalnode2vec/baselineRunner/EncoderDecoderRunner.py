#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import numpy
import pickle
import random 
import gensim
import numpy as np
import networkx as nx 
import tensorflow as tf
from gensim.models import Doc2Vec
from logManager.Logger import Logger
from word2vec.WordDoc2Vec import WordDoc2Vec
from baselineRunner.BaselineRunner import BaselineRunner 



label_node = lambda id_: 'V_%i' %(id_)



class EncoderDecoderRunner(BaselineRunner):
    def __init__(self, *args, **kwargs):
        self.reprName = "encoder_decoder"
        self.dataDir = os.environ['DATADIR']
        self.entities = list()
        self.paramDict = kwargs['pDict']

    def encoder(self, x):
        # Encoder Hidden layer with sigmoid activation #1
        layer_1 = tf.nn.sigmoid(tf.add(tf.matmul(x, self.weights['encoder_h1']),
                                       self.biases['encoder_b1']))
        # Encoder Hidden layer with sigmoid activation #2
        layer_2 = tf.nn.sigmoid(tf.add(tf.matmul(layer_1, self.weights['encoder_h2']),
                                       self.biases['encoder_b2']))
        return layer_2


    def decoder(self, x):
        # Decoder Hidden layer with sigmoid activation #1
        layer_1 = tf.nn.sigmoid(tf.add(tf.matmul(x, self.weights['decoder_h1']),
                                       self.biases['decoder_b1']))
        # Decoder Hidden layer with sigmoid activation #2
        layer_2 = tf.nn.sigmoid(tf.add(tf.matmul(layer_1, self.weights['decoder_h2']),
                                       self.biases['decoder_b2']))
        return layer_2


    def prepareData(self, graph_snapshot, snap_id):
        """
        """
        self.G = graph_snapshot
        self.entities = self.G.nodes()
        self.snapID = snap_id

    def learnEncoding(self, latent_space_size):

        os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
        graph = tf.Graph()
        tf.reset_default_graph()
        dataset_name = os.environ['DATASET']

        X_Train = nx.to_numpy_matrix(self.G, nodelist=self.entities, dtype=np.float32)

        X = tf.placeholder(tf.float32, shape= (len(self.entities), len(self.entities)))

        # tuning parameters
        learning_rate = 0.01
        num_steps = 10000
        #self.batch_size = 256

        # Network Parameters
        num_input = len(self.entities)
        num_hidden_1 = 256 
        num_hidden_2 = latent_space_size
         
        
        self.weights = {
            'encoder_h1': tf.Variable(tf.random_normal([num_input, num_hidden_1])),
            'encoder_h2': tf.Variable(tf.random_normal([num_hidden_1, num_hidden_2])),
            'decoder_h1': tf.Variable(tf.random_normal([num_hidden_2, num_hidden_1])),
            'decoder_h2': tf.Variable(tf.random_normal([num_hidden_1, num_input])),
        }

        self.biases = {
            'encoder_b1': tf.Variable(tf.random_normal([num_hidden_1])),
            'encoder_b2': tf.Variable(tf.random_normal([num_hidden_2])),
            'decoder_b1': tf.Variable(tf.random_normal([num_hidden_1])),
            'decoder_b2': tf.Variable(tf.random_normal([num_input])),
        }

        # Construct model
        encoder_op = self.encoder(X_Train)
        decoder_op = self.decoder(encoder_op)

        # Prediction
        y_pred = decoder_op
        # Targets (Labels) are the input data.
        y_true = X

        # Define loss and optimizer, minimize the squared error
        loss = tf.reduce_mean(tf.pow(y_true - y_pred, 2))
        optimizer = tf.train.RMSPropOptimizer(learning_rate).minimize(loss)

        # Initialize the variables (i.e. assign their default value)
        init = tf.global_variables_initializer()

        config=tf.ConfigProto(log_device_placement=True)
        config.gpu_options.allow_growth = True
        config.allow_soft_placement = True
        #config.device_count={'CPU': 1, 'GPU': 1}

        
        sess = tf.Session(config=config)

        sess.run(init) 

        for i in range(num_steps):
            sess.run(optimizer, feed_dict={X: X_Train} )

        encoded_vector = sess.run(encoder_op, feed_dict={X: X_Train})


        vDict = {}
        reprDict = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID),"wb") 
        
        pos = 0
        for entity in self.entities:
            vDict[entity] = encoded_vector[pos]
            #print (encoded_vector[pos].shape)
            pos = pos + 1
            
        pickle.dump (vDict, reprDict)

    def runTheBaseline(self, latent_space_size):
        dataset_name = os.environ['DATASET']

        self.learnEncoding(latent_space_size)

        