#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import numpy
import pickle
import random 
import gensim
import importlib
import numpy as np 
import networkx as nx 
import tensorflow as tf
from gensim.models import Doc2Vec
from logManager.Logger import Logger
from word2vec.WordDoc2Vec import WordDoc2Vec
from baselineRunner.BaselineRunner import BaselineRunner 
from evaluation.classificationevaluation.ClassificationEvaluationLinkPrediction import ClassificationEvaluationLinkPrediction
from evaluation.classificationevaluation.ClassificationEvaluationLinkForecast import ClassificationEvaluationLinkForecast


module_dict = {"deepwalk": "baselineRunner.DeepWalkRunner",\
         "node2vec": "baselineRunner.Node2VecRunner",\
         "line" : "baselineRunner.LineRunner",\
         "tsvd":"baselineRunner.TruncatedSVDRunner",\
         "pca": "baselineRunner.PCARunner",\
         "spectral": "baselineRunner.SpectralClusteringRunner",\
         "modularity": "baselineRunner.ModularityMaximizationRunner",\
         "lle": "baselineRunner.LLERunner",\
         "isomap":"baselineRunner.IsoMapRunner",\
         "mds": "baselineRunner.MDS",\
         "encoder_decoder":"baselineRunner.EncoderDecoderRunner",\
         "netmf":"baselineRunner.NetMFRunner"}

class_dict = {"deepwalk":"DeepWalkRunner", "pca":"PCARunner",\
            "node2vec": "Node2VecRunner",\
            "line": "LineRunner",\
            "tsvd":"TruncatedSVDRunner",\
            "spectral": "SpectralClusteringRunner",\
            "modularity": "ModularityMaximizationRunner",\
            "lle": "LLERunner",\
            "isomap": "IsoMapRunner",\
            "mds": "MDSRunner",\
            "encoder_decoder":"EncoderDecoderRunner",\
            "netmf":"NetMFRunner" }


class HomogeneousLinearProjectionRunner(BaselineRunner):
    def __init__(self, *args, **kwargs):
        """
        """
        self.reprName = "homogeneous_linear_projection"
        self.paramDict = kwargs['paramDict']
        self.method = self.paramDict['method']
        
        module = module_dict[self.method]
        klass  = class_dict[self.method]
        self.Klass = getattr(importlib.import_module(module), klass)
        self.dataDir = os.environ['DATADIR']

    def prepareData(self, nSlots, entities):
        """
        """
        pass 

    def getTrainingData(self, latent_space_size):
        """
        """
        dataset_name = os.environ['DATASET']
        reprName = self.reprName
        x = numpy.zeros(shape=(1,latent_space_size))
        z = numpy.zeros(shape=(1,latent_space_size))

        st_snap_id = int(os.environ['STSNAPID'])
        end_snap_id = int(os.environ['ENDSNAPID'])

        for snapID in range(st_snap_id, end_snap_id):
            Logger.logr.info ("concatenating %ith snapshop"%snapID)
            reprFile = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                  dataset_name, self.method, snapID), "rb")
            vDict = pickle.load (reprFile)

            for entity in self.entities:
                ent_vector = vDict[entity].reshape(1, latent_space_size)
                if snapID > st_snap_id and snapID <= end_snap_id - 2:
                    x = np.vstack((x, ent_vector))
                    z = np.vstack((z, ent_vector))
                if snapID == st_snap_id:
                    x = np.vstack((x, ent_vector))
                if snapID == end_snap_id -1:
                    z = np.vstack((z, ent_vector))

            Logger.logr.info ("Returning x of shape = %s and z of shape = %s"\
                %(x.shape, z.shape)) 

        x = np.delete(x,0,0)
        z = np.delete(z,0,0)


        Logger.logr.info ("Returning x of shape = %s and z of shape = %s"\
                %(x.shape, z.shape))

        return x, z 


    def getLinearProjectionVector(self, latent_space_size):
        """
        """
        # Disable warnings
        os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
        graph = tf.Graph()
        tf.reset_default_graph()

        x, z = self.getTrainingData(latent_space_size)

        init = tf.constant(np.random.rand(latent_space_size, latent_space_size).astype(np.float32))
        W = tf.get_variable('W', initializer=init)


        x_place = tf.placeholder(tf.float32, shape= (latent_space_size, x.shape[0]))
        linear_model = tf.matmul(tf.transpose(W), x_place)


        z_place = tf.placeholder(tf.float32, shape = (latent_space_size, x.shape[0]))
        loss = tf.reduce_sum(tf.square(tf.subtract(linear_model, z_place)))
        optimizer = tf.train.GradientDescentOptimizer(0.5)


        gradients, variables = zip(*optimizer.compute_gradients(loss))
        gradients, _ = tf.clip_by_global_norm(gradients, 5.0)
        optimize = optimizer.apply_gradients(zip(gradients, variables))


        init = tf.global_variables_initializer()
        sess = tf.Session()


        x_train = x.transpose()
        z_train = z.transpose()

        #np.savetxt('test.out', x_train.transpose(), delimiter=',', fmt='%.4f')
        #np.savetxt('test_z.out', z_train.transpose(), delimiter=",", fmt='%.4f') 

        Logger.logr.info ("Shape of x_train = %s and z_train = %s"%(x_train.shape, z_train.shape))

        sess.run(init) 

        for i in range(1000):
            sess.run(optimize, {x_place: x_train, z_place: z_train})

        curr_W, curr_loss = sess.run([W, loss], {x_place: x_train, z_place: z_train})


        return np.dot(curr_W.transpose(), z_train[:,-(len(self.entities)):])


    def runTheBaseline(self):
        """
        """
        nslots = int(os.environ['NSLOTS'])
        param_dict_list = self.get_param_dict_list()

        st_snap_id = int(os.environ['STSNAPID'])
        end_snap_id = int(os.environ['ENDSNAPID'])

        dataset_name = os.environ['DATASET']
        dirname = os.path.join(self.dataDir,dataset_name)
        latSpaceSize = int(os.environ['latSize'])

        result_file = open(os.environ['RES_FILE'], "a")
        param_file = open(os.environ['PARAM_FILE'], "a")
        result_trace_file = open(os.environ['RES_TRACE_FILE'], "a")

        latSpaceSize = int(os.environ['latSize'])

        optlist = []
        os.environ['TEST_MODE']='VALID'

        
        self.entities = []

        for pos in range(0, len(param_dict_list)):
            Logger.logr.info ("Working for param=%s"%str(param_dict_list[pos]))
            for snapID in range(st_snap_id, end_snap_id):
                runner = self.Klass(pDict=param_dict_list[pos])
                self.reprName = "%s_%s"%(runner.reprName, "homogeneous_linear_projection")
                g = nx.read_gpickle(os.path.join(dirname,\
                     "graph_%i"%snapID))
                if len(self.entities) == 0:
                    self.entities = g.nodes() 
                runner.prepareData(g, snapID)
                runner.runTheBaseline(latSpaceSize)


            projected_matrix =  (self.getLinearProjectionVector(latSpaceSize)).transpose()
            vDict = {}
            reprDict = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                      dataset_name, self.reprName, end_snap_id),"wb") 
          
            index = 0
            for entity in self.entities:
                 vDict[entity] = projected_matrix[index]
                 index = index + 1          
               
            Logger.logr.info (reprDict)
            filename = "%s/%s/%s_repr_%i.p"%(self.dataDir,\
                      dataset_name, self.reprName, end_snap_id)
            Logger.logr.info (os.path.getsize(filename))

            pickle.dump (vDict, reprDict, protocol=pickle.HIGHEST_PROTOCOL)


            snapID_to_eval = end_snap_id  
            classeval = ""

            if os.environ['TASK'] == 'PRED':
                classeval = ClassificationEvaluationLinkPrediction(isMerged=False) 
            else:
                classeval = ClassificationEvaluationLinkForecast(isMerged=False)

            classeval.generateData(snapID_to_eval, self.reprName,\
                 os.environ['COMPOSITION'])
            acc, auc, auprc, ndcg = classeval.runClassificationTask(snapID_to_eval,\
                 self.reprName)
            optlist.append ((param_dict_list[pos], ndcg))  



        param_file.write ("%s%s"%(self.reprName, os.linesep))
        for pos in range(0, len(optlist)):
            param_file.write ("%s%s"%(str(optlist[pos][0]), os.linesep))
            param_file.write ("ndcg value:%s%s"%(str(optlist[pos][1]), os.linesep))


        pos,val = self.getOptPos(optlist)
        pdict = param_dict_list[pos]


        param_file.write("Optimal Param%s"%os.linesep)
        param_file.write("pdict=%s%s"%(str(pdict),os.linesep))
        os.environ['TEST_MODE']='TEST'
        

        acc_list, auc_list, auprc_list, ndcg_list = [], [], [], []

        for pos in range (0, int(os.environ['REPEAT'])):   
            for snapID in range(st_snap_id, end_snap_id):
                runner = self.Klass(pDict=pdict)
                Logger.logr.info ("Working for param=%s"%str(pdict))
                self.reprName = "%s_%s"%(runner.reprName, "homogeneous_linear_projection")
                g = nx.read_gpickle(os.path.join(dirname,\
                     "graph_%i"%snapID))
                if len(self.entities) == 0:
                    self.entities = g.nodes() 
                runner.prepareData(g, snapID)
                runner.runTheBaseline(latSpaceSize)


            projected_matrix =  (self.getLinearProjectionVector(latSpaceSize)).transpose()
            vDict = {}
            reprDict = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                      dataset_name, self.reprName, end_snap_id),"wb") 
          
            index = 0
            for entity in self.entities:
                 vDict[entity] = projected_matrix[index]
                 #vec_str = self.convert_to_str(vDict[entity])
                 index = index + 1          
               
            pickle.dump (vDict, reprDict, protocol=pickle.HIGHEST_PROTOCOL)   

            snapID_to_eval = end_snap_id  

            classeval = ""
            if os.environ['TASK'] == 'PRED': 
                classeval = ClassificationEvaluationLinkPrediction(isMerged=False) 
            else:
                classeval = ClassificationEvaluationLinkForecast(isMerged=False)
            classeval.generateData(snapID_to_eval, self.reprName,\
                 os.environ['COMPOSITION'])
            acc, auc, auprc, ndcg = classeval.runClassificationTask(snapID_to_eval,\
                 self.reprName)
            auc_list.append(auc)
            acc_list.append(acc)
            auprc_list.append(auprc)
            ndcg_list.append(ndcg)

        self.writeResults(result_file, result_trace_file,\
                    acc_list, auc_list, auprc_list, ndcg_list)
                    