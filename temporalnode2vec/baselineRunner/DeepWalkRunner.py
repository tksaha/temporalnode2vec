#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import numpy
import pickle
import random 
import gensim
import networkx as nx 
from gensim.models import Doc2Vec
from logManager.Logger import Logger
from word2vec.WordDoc2Vec import WordDoc2Vec
from baselineRunner.BaselineRunner import BaselineRunner 



label_node = lambda id_: 'V_%i' %(id_)


"""
From node representation, we can find edge representation by 
performing following operation: average, hadamard (mult), 
weighted L1 and weighted L2 distance. (Reference: node2vec 
paper)
"""

class DeepWalkRunner(BaselineRunner):
    def __init__(self, *args, **kwargs):
        self.reprName = "deepwalk"
        self.dataDir = os.environ['DATADIR']
        self.num_walk = int(os.environ['NUM_WALK'])
        self.walk_length = int(os.environ['WALK_LEN'])
        self.paramDict = kwargs['pDict']
        self.window_size = self.paramDict['window_size']
        self.random_value = random.Random(0)
        try: 
            self.random_value = self.paramDict['random']
        except:
            pass
        self.entities = list()
        self.G = nx.Graph()
        self.snapID = -1

    def prepareData(self, graph_snapshot, snap_id):
        """
        """
        self.G = graph_snapshot
        self.entities = self.G.nodes()
        self.snapID = snap_id

    def random_walk(self, G, path_length, alpha,\
             rand, start=None):
        """ 
        Returns a truncated random walk.
        path_length: Length of the random walk.
        alpha: probability of restarts.
        start: the start node of the random walk.
        """
        if  start is not None:
            path = [start]
        else:
            # Sampling is uniform w.r.t V, and not w.r.t E
            path = [rand.choice(G.nodes())]

        while len(path) < path_length:
            cur = path[-1]
            if  len(G.neighbors(cur)) > 0:
                if rand.random() >= alpha:
                    path.append(rand.choice(G.neighbors(cur)))
                else:
                    path.append(path[0])
            else:
                break
        return path


    def build_deepwalk_corpus_iter(self, G, num_paths,\
         path_length, alpha,rand):

        walks = []
        nodes = list(G.nodes())

        for cnt in range(num_paths):
            rand.shuffle(nodes)
            for node in nodes:
                yield self.random_walk(G, path_length,\
                     rand=self.random_value, alpha=alpha, start=node)


    def runTheBaseline(self, latent_space_size):
        dataset_name = os.environ['DATASET']

        file_name = "%s/%s/%s_corpus_%i.txt"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID)
        dirname = "%s/%s"%(self.dataDir,dataset_name)

        if  not (os.path.exists (dirname)): 
            os.makedirs(dirname)

        file_to_save = open (file_name, "w")

        # Graph, num_walk, walk_length
        for path in self.build_deepwalk_corpus_iter(self.G,\
                     self.num_walk, self.walk_length, 0.0, self.random_value):          
            file_to_save.write(' '.join([str(label_node(nodes)) for nodes in path]))
            file_to_save.write(os.linesep)
        file_to_save.flush()
        file_to_save.close()

        training_file = "%s/%s/%s_corpus_%i.txt"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID)
        output_file = "%s/%s/%s_repr_%i.txt"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID)

        wordDoc2Vec = WordDoc2Vec()
        wPDict = wordDoc2Vec.buildWordDoc2VecParamDict()
        
        wPDict["cbow"],wPDict["min-count"] = str(0), str(0)
        wPDict["train"], wPDict["output"] = training_file, output_file
        wPDict["size"], wPDict["sentence-vectors"] = str(latent_space_size),\
                     str(0)
        wPDict["window"] = str(self.window_size)
        wPDict['hs'] = str(1)
        wPDict['negative'] = str(0)


        args = wordDoc2Vec.buildArgListforW2V(wPDict)
        self.runProcess(args)

        vDict = {}
        vecModel = gensim.models.KeyedVectors.\
                    load_word2vec_format(output_file, binary=False)
        reprDict = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID),"wb") 
        
        for entity in self.entities:
            vDict[entity] = vecModel[label_node(entity)]
            vec_str = self.convert_to_str(vDict[entity])
            
        pickle.dump (vDict, reprDict, protocol=pickle.HIGHEST_PROTOCOL)
        