#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import pickle
import random 
import gensim
import math
import scipy.io
import numpy as np
import networkx as nx 
from sklearn.decomposition import NMF
import scipy.sparse as sparse
from scipy.sparse import csgraph
from gensim.models import Doc2Vec
from logManager.Logger import Logger
from word2vec.WordDoc2Vec import WordDoc2Vec
from baselineRunner.BaselineRunner import BaselineRunner 
from sklearn.utils import check_random_state
from evaluation.classificationevaluation.ClassificationEvaluationLinkPrediction import ClassificationEvaluationLinkPrediction
from evaluation.classificationevaluation.ClassificationEvaluationLinkForecast import ClassificationEvaluationLinkForecast
from sklearn.preprocessing import normalize

label_node = lambda id_: 'V_%i' %(id_)


class GrNMFRunner(BaselineRunner):
    def __init__(self, *args, **kwargs):
        self.reprName = "grnmf"
        self.dataDir = os.environ['DATADIR']
        self.paramDict = kwargs['paramDict']
        self.EPSILON = np.finfo(np.float32).eps
        
    def prepareData(self, graph_snapshot, snap_id):
        """
        """
        pass 

    def init_factor_matrices(self, X, n_components):
        """
        https://github.com/scikit-learn/scikit-learn/blob/master/sklearn/decomposition/nmf.py
        """
        n_samples, n_features = X.shape
        sum_ = np.sqrt(X.sum() / n_components)

        rng = check_random_state(1000)
        H =  rng.randn(n_components, n_features)
        W =  rng.randn(n_samples, n_components)
        # we do not write np.abs(H, out=H) to stay compatible with
        # numpy 1.5 and earlier where the 'out' keyword is not
        # supported as a kwarg on ufuncs
        np.abs(H, H)
        np.abs(W, W)
        return W, H

    def grnmf(self, X, L,latSpaceSize, alpha):
        # Calculate L

        B, F = self.init_factor_matrices (X, latSpaceSize)
        for i in range(0, 5):
            bdash_wt = np.matmul(B.transpose(), X)
            bdashff = np.matmul(np.matmul(B.transpose(), B), F)
            #alphaFl = alpha * np.matmul(F, L)
            #divider = bdashff + alphaFl
            divider = bdashff
            divider [divider<=1e-10] = 1e-10
            F = np.multiply(bdash_wt/(divider), F)

            wtf_dash = np.matmul(X, F.transpose())
            bff = np.matmul(np.matmul(B, F), F.transpose())
            bff[bff<=1e-10] = 1e-10
            B = np.multiply(wtf_dash/bff, B)


        return normalize(B,axis=0)

    
    def runTheBaseline(self):
        dataset_name = os.environ['DATASET']
        dirname = "%s/%s"%(self.dataDir,dataset_name)

        nslots = int(os.environ['NSLOTS'])
        param_dict_list = self.get_param_dict_list()

        st_snap_id = int(os.environ['STSNAPID'])
        end_snap_id = int(os.environ['ENDSNAPID'])

        dataset_name = os.environ['DATASET']
        dirname = os.path.join(self.dataDir,dataset_name)
        latSpaceSize = int(os.environ['latSize'])

        result_file = open(os.environ['RES_FILE'], "a")
        param_file = open(os.environ['PARAM_FILE'], "a")
        result_trace_file = open(os.environ['RES_TRACE_FILE'], "a")

        latSpaceSize = int(os.environ['latSize'])

        optlist = []
        os.environ['TEST_MODE']='VALID'

        
        self.entities = []

        for pos in range(0, len(param_dict_list)):
            Logger.logr.info ("Working for param=%s"%str(param_dict_list[pos]))

            L = ""
            theta = param_dict_list[pos]['theta']
            alpha = param_dict_list[pos]['alpha']
            for snapID in range(st_snap_id, end_snap_id-1):   
                g = nx.read_gpickle(os.path.join(dirname,\
                     "graph_%i"%snapID))
                if  snapID == st_snap_id:
                    self.entities = g.nodes ()
                    lx = nx.laplacian_matrix (g, self.entities).todense()
                    L = math.pow(theta,float(end_snap_id-2 -snapID)) * lx 
                else:
                    lx = nx.laplacian_matrix (g, self.entities).todense()
                    L = L + math.pow(theta,float(end_snap_id-2 -snapID)) * lx 

            g = nx.read_gpickle(os.path.join(dirname,\
                     "graph_%i"%(end_snap_id-1)))

            X = nx.to_numpy_matrix(g, nodelist=self.entities)
            outfile = "z.csv"
            np.savetxt(outfile, X, delimiter=",", fmt='%.0f')

            # Call NMF
            model = NMF(n_components=latSpaceSize, init='random', random_state=0)

            #B = np.array(self.grnmf(X, L, latSpaceSize, alpha))
            B = model.fit_transform(X)
            np.savetxt(outfile, X, delimiter=",", fmt='%.9f')

            index = 0
            vDict = {}
            reprDict = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                      dataset_name, self.reprName, end_snap_id-1),"wb")


            for entity in self.entities:
                 vDict[entity] = B[index]
                 index = index + 1          
               
            pickle.dump (vDict, reprDict)


            snapID_to_eval = end_snap_id  
            classeval = ""

            if os.environ['TASK'] == 'PRED':
                classeval = ClassificationEvaluationLinkPrediction(isMerged=False) 
            else:
                classeval = ClassificationEvaluationLinkForecast(isMerged=False)

            classeval.generateData(snapID_to_eval, self.reprName,\
                 os.environ['COMPOSITION'])
            acc, auc, auprc, ndcg = classeval.runClassificationTask(snapID_to_eval,\
                 self.reprName)
            optlist.append ((param_dict_list[pos], ndcg)) 