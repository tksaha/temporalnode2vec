#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import numpy as np
import pickle
import networkx as nx 
from sklearn.decomposition import PCA
from baselineRunner.BaselineRunner import BaselineRunner


class PCARunner(BaselineRunner):
    def __init__(self, *args, **kwargs):
        self.reprName = "pca"
        self.dataDir = os.environ['DATADIR']
        self.entities = list()
        self.snapID = 1
        self.G = nx.Graph()

    def prepareData(self, graph_snapshot, snap_id):
        """
        """
        self.G = graph_snapshot
        self.entities = self.G.nodes()
        self.snapID = snap_id
    

    def runTheBaseline(self, latent_space_size):
        """
        """
        dataset_name = os.environ['DATASET']

        def remove_nodes_with_no_edges():
            reduced_graph = nx.Graph()
            for node in self.G.nodes():
                if len(self.G.neighbors(node)) > 0:
                    reduced_graph.add_node(node)
            for edge in self.G.edges():
                reduced_graph.add_edge(edge[0], edge[1])
            return reduced_graph

        reduced_graph = remove_nodes_with_no_edges()
        matrix = nx.adjacency_matrix(reduced_graph, reduced_graph.nodes()).todense()

        # Run PCA 
        vDict = {}
        pca = PCA(n_components=latent_space_size)
        matrix_transformed = pca.fit_transform(matrix)

        
        reprFile = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID),"wb") 


        reduced_graph_entities = reduced_graph.nodes()
        for entity in range(0, len(reduced_graph.nodes())):
            vec_ = matrix_transformed[entity]
            vDict[reduced_graph_entities[entity]] = vec_ 
            
        for entity in self.entities:
            if entity not in vDict:
                vDict[entity] = np.zeros(latent_space_size)


        pickle.dump(vDict, reprFile)    
        reprFile.close()

        
    def runEvaluationTask(self):
        """
        """
        pass 