#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import pickle
import numpy
import networkx as nx 
from sklearn import manifold
from baselineRunner.BaselineRunner import BaselineRunner


class LLERunner(BaselineRunner):
    def __init__(self, *args, **kwargs):
        self.reprName = "lle"
        self.dataDir = os.environ['DATADIR']
        self.entities = list()
        self.snapID = 1
        self.G = nx.Graph()
        self.knbr = 20

    def prepareData(self, graph_snapshot, snap_id):
        """
        """
        self.G = graph_snapshot
        self.entities = self.G.nodes()
        self.snapID = snap_id


    def runTheBaseline(self, latent_space_size):
        """
        """
        dataset_name = os.environ['DATASET']
        matrix = nx.adjacency_matrix(self.G, self.entities).todense()
    
        # It uses eucledian distance for neighborhood graph computation
        vDict = {}
        matrix_transformed = manifold.LocallyLinearEmbedding(self.knbr, latent_space_size,
                                        eigen_solver='auto',
                                        method='standard').fit_transform(matrix)

        
        reprFile = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID),"wb") 

        for entity in range(0, len(self.entities)):
            vec_ = matrix_transformed[entity]
            vDict[self.entities[entity]] = vec_ 
            

        pickle.dump(vDict, reprFile)    
        reprFile.close()
        
    def runEvaluationTask(self):
        """
        """
        pass