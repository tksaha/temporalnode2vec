#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import subprocess
import numpy as np
from abc import ABCMeta, abstractmethod
from logManager.Logger import Logger



class BaselineRunner:
    def __init__(self, *args, **kwargs):
        pass 

    @abstractmethod
    def prepareData(self):
        """
        """
        pass

    @abstractmethod
    def runTheBaseline(self):
        """
        """
        pass

    def convert_to_str(self, vec):
        str_ = ""
        for val in vec: 
            str_ ="%s %0.4f"%(str_,val)
        return str_

    def get_param_dict_list(self):
        all_param_keys = []
        for key in self.paramDict.keys():
            if 'method' not in key:
                all_param_keys.append(key)

        param_dict_list = []
        first_key = True
        for key in all_param_keys:
            list_pos = 0
            # print (key)
            for val in self.paramDict[key]:
                if  first_key == True:
                    new_dict = {}
                    new_dict[key] = val 
                    param_dict_list.append(new_dict)
                else:
                    # print (list_pos, key)
                    param_dict_list[list_pos][key] = val
                list_pos = list_pos + 1

            if first_key == True:
                first_key = False
        return param_dict_list

    def getOptPos(self, optlist):
        arg_id = 0 
        arg_val = optlist[arg_id][1]
 
        for pos in range(1, len(optlist)):
            if optlist[pos][1] > arg_val:
                arg_id = pos 
                arg_val = optlist[pos][1]

        return arg_id, arg_val

    def writeResults(self, result_file,result_trace_file,\
            acc_list,auc_list,auprc_list, ndcg_list ):

        reprName = self.reprName
        if  "heterogeneous" in reprName:
            reprName = "%s_%s"%(reprName, os.environ['HETER_COMB'])

        result_file.write("%s,%s,%s,%s,%s%s"%(reprName,\
             np.average(acc_list), np.average(auc_list),\
             np.average(auprc_list), np.average(ndcg_list), os.linesep))
        result_file.write("%s,%s,%s,%s,%s%s"%(reprName,\
             np.std(acc_list), np.std(auc_list),\
             np.std(auprc_list), np.std(ndcg_list), os.linesep))

        # Tracing data for statistical test and for manual checking
        acc_str = ",".join(list(map(str,acc_list)))
        result_trace_file.write("%s,%s,%s%s"%\
                (reprName, "acc",acc_str,os.linesep))
        result_trace_file.write("%s,%s,%s%s"%\
                (reprName, "auc",",".join(list(map(str,auc_list))),\
                 os.linesep))
        result_trace_file.write("%s,%s,%s%s"%\
                 (reprName, "auprc",",".join(list(map(str,auprc_list))),\
                 os.linesep))
        result_trace_file.write("%s,%s,%s%s"%\
                 (reprName, "ndcg",",".join(list(map(str,ndcg_list))),\
                  os.linesep))

    def runProcess (self,args): 
        
        Logger.logr.info(args)


        proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = proc.communicate()
        if  proc.returncode != 0: 
            Logger.logr.error("Process haven't terminated successfully")
            Logger.logr.info(args)
            Logger.logr.info(out)
            Logger.logr.info(err)
            sys.exit(1)
      
            