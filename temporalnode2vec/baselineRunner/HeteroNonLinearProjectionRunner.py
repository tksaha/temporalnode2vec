#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import math 
import sys 
import numpy
import pickle
import random 
import gensim
import importlib
import numpy as np 
import networkx as nx 
import tensorflow as tf
from gensim.models import Doc2Vec
from logManager.Logger import Logger
from word2vec.WordDoc2Vec import WordDoc2Vec
from baselineRunner.BaselineRunner import BaselineRunner 
from evaluation.classificationevaluation.ClassificationEvaluationLinkPrediction import ClassificationEvaluationLinkPrediction
from evaluation.classificationevaluation.ClassificationEvaluationLinkForecast import ClassificationEvaluationLinkForecast

module_dict = {"deepwalk": "baselineRunner.DeepWalkRunner",\
         "node2vec": "baselineRunner.Node2VecRunner",\
         "line" : "baselineRunner.LineRunner",\
         "tsvd":"baselineRunner.TruncatedSVDRunner",\
         "pca": "baselineRunner.PCARunner",\
         "spectral": "baselineRunner.SpectralClusteringRunner",\
         "modularity": "baselineRunner.ModularityMaximizationRunner",\
         "lle": "baselineRunner.LLERunner",\
         "isomap":"baselineRunner.IsoMapRunner",\
         "mds": "baselineRunner.MDS",\
         "encoder_decoder":"baselineRunner.EncoderDecoderRunner"}

class_dict = {"deepwalk":"DeepWalkRunner", "pca":"PCARunner",\
            "node2vec": "Node2VecRunner",\
            "line": "LineRunner",\
            "tsvd":"TruncatedSVDRunner",\
            "spectral": "SpectralClusteringRunner",\
            "modularity": "ModularityMaximizationRunner",\
            "lle": "LLERunner",\
            "isomap": "IsoMapRunner",\
            "mds": "MDSRunner",\
            "encoder_decoder":"EncoderDecoderRunner" }


class HeteroNonLinearProjectionRunner(BaselineRunner):
    def __init__(self, *args, **kwargs):
        """
        """
        self.reprName = "heterogeneous_nonlinear_projection"
        self.paramDict = kwargs['paramDict']
        self.method = self.paramDict['method']
        
        module = module_dict[self.method]
        klass  = class_dict[self.method]
        self.Klass = getattr(importlib.import_module(module), klass)
        self.dataDir = os.environ['DATADIR']

        

    def prepareData(self, nSlots, entities):
        """
        """
        pass

    def reset_graph(self):
        graph = tf.Graph()
        tf.reset_default_graph()

    def getTrainingData(self, latent_space_size, start_time, end_time):
        """
        """
        dataset_name = os.environ['DATASET']
        reprName = self.reprName

        x = numpy.zeros(shape=(1,latent_space_size))
        z = numpy.zeros(shape=(1,latent_space_size))



        for snapID in range(start_time, end_time+1):
            reprFile = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                  dataset_name, self.method, snapID), "rb")
            vDict = pickle.load (reprFile)

            for entity in self.entities:
                ent_vector = vDict[entity].reshape(1, latent_space_size)
                if snapID == start_time:
                    x = np.vstack((x, ent_vector))
                if snapID == end_time:
                    z = np.vstack((z, ent_vector))

            Logger.logr.info ("Returning x of shape = %s and z of shape = %s"\
                %(x.shape, z.shape)) 

        x = np.delete(x,0,0)
        z = np.delete(z,0,0)


        Logger.logr.info ("Returning x of shape = %s and z of shape = %s"\
                %(x.shape, z.shape))

        return x, z 


    def getLinearProjectionVector(self, latent_space_size, start_time, end_time):
        """
        """
        # Disable warnings
        os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
       

        x, z = self.getTrainingData(latent_space_size, start_time, end_time)

        init = tf.constant(np.random.rand(latent_space_size, latent_space_size).astype(np.float32))
        W = tf.get_variable('W', initializer=init)


        x_place = tf.placeholder(tf.float32, shape= (latent_space_size, x.shape[0]))
        linear_model = tf.matmul(tf.transpose(W), x_place)
        nonlinear_model = linear_model # should be replace later

        if  os.environ['NLIN']  == 'relu':
            nonlinear_model = tf.nn.relu(linear_model)
            if  "relu" not in self.reprName:
                self.reprName = "%s_relu"%(self.reprName)
        elif os.environ['NLIN'] == 'sigmoid':
            nonlinear_model = tf.nn.sigmoid(linear_model)
            if  "sigmoid" not in self.reprName:
                self.reprName = "%s_sigmoid"%(self.reprName)
        elif os.environ['NLIN'] == 'tanh':
            nonlinear_model = tf.nn.tanh(linear_model)
            if  "tanh" not in self.reprName:
                self.reprName = "%s_tanh"%(self.reprName)
        else:
            Logger.logr.info ("Need to specify non-linearity")
            sys.exit(1)

        z_place = tf.placeholder(tf.float32, shape = (latent_space_size, x.shape[0]))
        loss = tf.reduce_sum(tf.square(tf.subtract(nonlinear_model, z_place)))
        optimizer = tf.train.GradientDescentOptimizer(0.5)


        gradients, variables = zip(*optimizer.compute_gradients(loss))
        gradients, _ = tf.clip_by_global_norm(gradients, 5.0)
        optimize = optimizer.apply_gradients(zip(gradients, variables))


        init = tf.global_variables_initializer()
        sess = tf.Session()


        x_train = x.transpose()
        z_train = z.transpose()

        #np.savetxt('test.out', x_train.transpose(), delimiter=',', fmt='%.4f')
        #np.savetxt('test_z.out', z_train.transpose(), delimiter=",", fmt='%.4f') 

        Logger.logr.info ("Shape of x_train = %s and z_train = %s"%(x_train.shape, z_train.shape))

        sess.run(init) 

        # Take number of iteration as parameter
        for i in range(1000):
            sess.run(optimize, {x_place: x_train, z_place: z_train})

        curr_W, curr_loss = sess.run([W, loss], {x_place: x_train, z_place: z_train})


        return curr_W


    def runTheBaseline(self):
        
        nslots = int(os.environ['NSLOTS'])
        param_dict_list = self.get_param_dict_list()

        st_snap_id = int(os.environ['STSNAPID'])
        end_snap_id = int(os.environ['ENDSNAPID'])

        dataset_name = os.environ['DATASET']
        dirname = os.path.join(self.dataDir,dataset_name)
        latSpaceSize = int(os.environ['latSize'])

        result_file = open(os.environ['RES_FILE'], "a")
        param_file = open(os.environ['PARAM_FILE'], "a")
        result_trace_file = open(os.environ['RES_TRACE_FILE'], "a")

     

        optlist = []
        os.environ['TEST_MODE']='VALID'

        st_snap_id  = int(os.environ['STSNAPID'])
        end_snap_id = int(os.environ['ENDSNAPID'])

        self.entities = []

        for pos in range(0, len(param_dict_list)):
            Logger.logr.info ("Working for param=%s"%str(param_dict_list[pos]))
            self.reset_graph()


            for snapID in range(st_snap_id, end_snap_id):
                runner = self.Klass(pDict=param_dict_list[pos])
                if  runner.reprName not in self.reprName:
                    self.reprName = "%s_%s"%(runner.reprName, self.reprName)
                g = nx.read_gpickle(os.path.join(dirname,\
                     "graph_%i"%snapID))
                if len(self.entities) == 0:
                    self.entities = g.nodes() 
                runner.prepareData(g, snapID)
                runner.runTheBaseline(latSpaceSize)


            curr_W = np.zeros((latSpaceSize, latSpaceSize)).astype(np.float32)
            hmany = 0
            with tf.variable_scope("model") as scope:
                for snapID in range(st_snap_id, end_snap_id-1):
                    Logger.logr.info("Working for snapid = %i"%snapID)
                    frac_w = 1.0
                    if  os.environ['HETER_COMB'] == 'linear':
                        frac_w = (snapID - st_snap_id +1) / (end_snap_id-st_snap_id-1) 
                    elif os.environ['HETER_COMB'] == 'exp':
                        frac_w = math.exp((snapID - st_snap_id +1) / (end_snap_id-st_snap_id-1))
                    elif os.environ['HETER_COMB'] == 'wct':
                        theta = param_dict_list[pos]['theta']
                        frac_w = math.pow((1.0 - theta),(end_snap_id-2 -snapID))
    
                    curr_W = curr_W + frac_w * self.getLinearProjectionVector (latSpaceSize, snapID, snapID+1)
                    hmany = hmany + 1
                    scope.reuse_variables()

            if  os.environ['HETER_COMB'] ==  'avg':
                curr_W = curr_W/hmany 

            x, z = self.getTrainingData(latSpaceSize, end_snap_id-2, end_snap_id-1)
            z_train = z.transpose()

            proj_mat = np.dot(curr_W.transpose(), z_train)
            projected_matrix = proj_mat.transpose()
            

            vDict = {}
            reprDict = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                      dataset_name, self.reprName, end_snap_id),"wb") 
          
            index = 0
            for entity in self.entities:
                 vDict[entity] = projected_matrix[index]
                 vec_str = self.convert_to_str(vDict[entity])
                 index = index + 1          
               
            pickle.dump (vDict, reprDict)
            
            snapID_to_eval = end_snap_id  
            classeval = ""

            if os.environ['TASK'] == 'PRED':
                classeval = ClassificationEvaluationLinkPrediction(isMerged=False) 
            else:
                classeval = ClassificationEvaluationLinkForecast(isMerged=False)

            classeval.generateData(snapID_to_eval, self.reprName,\
                 os.environ['COMPOSITION'])
            acc, auc, auprc, ndcg = classeval.runClassificationTask(snapID_to_eval,\
                 self.reprName)
            optlist.append ((param_dict_list[pos], ndcg))



        param_file.write ("%s%s"%(self.reprName, os.linesep))
        for pos in range(0, len(optlist)):
            param_file.write ("%s%s"%(str(optlist[pos][0]), os.linesep))
            param_file.write ("ndcg value:%s%s"%(str(optlist[pos][1]), os.linesep))


        pos,val = self.getOptPos(optlist)
        pdict = param_dict_list[pos]


        param_file.write("Optimal Param%s"%os.linesep)
        param_file.write("pdict=%s%s"%(str(pdict),os.linesep))
        os.environ['TEST_MODE']='TEST'
        

        acc_list, auc_list, auprc_list, ndcg_list = [], [], [], []
        for pos in range (0, int(os.environ['REPEAT'])):  
            self.reset_graph() 
            for snapID in range(st_snap_id, end_snap_id):
                runner = self.Klass(pDict=pdict)
                if  runner.reprName not in self.reprName:
                    self.reprName = "%s_%s"%(runner.reprName, self.reprName)
                g = nx.read_gpickle(os.path.join(dirname,\
                     "graph_%i"%snapID))
                if len(self.entities) == 0:
                    self.entities = g.nodes() 
                runner.prepareData(g, snapID)
                runner.runTheBaseline(latSpaceSize)


            curr_W = np.zeros((latSpaceSize, latSpaceSize)).astype(np.float32)
            hmany = 0
            with tf.variable_scope("model") as scope:
                for snapID in range(st_snap_id, end_snap_id-1):
                    Logger.logr.info("Working for snapid = %i"%snapID)
                    frac_w = 1.0
                    if  os.environ['HETER_COMB'] == 'linear':
                        frac_w = (snapID - st_snap_id +1) / (end_snap_id-st_snap_id-1)
                    elif os.environ['HETER_COMB'] == 'exp':
                        frac_w = math.exp((snapID - st_snap_id +1) / (end_snap_id-st_snap_id-1))
                    elif os.environ['HETER_COMB'] == 'wct':
                        theta = pdict['theta']
                        frac_w = math.pow((1.0 - theta),(end_snap_id-2 -snapID))
    
                    curr_W = curr_W + frac_w * self.getLinearProjectionVector (latSpaceSize, snapID, snapID+1)
                    hmany = hmany + 1
                    scope.reuse_variables()

            if  os.environ['HETER_COMB'] ==  'avg':
                curr_W = curr_W/hmany 

            x, z = self.getTrainingData(latSpaceSize, end_snap_id-2, end_snap_id-1)
            z_train = z.transpose()

            proj_mat = np.dot(curr_W.transpose(), z_train)
            projected_matrix = proj_mat.transpose()
            

            vDict = {}
            reprDict = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                      dataset_name, self.reprName, end_snap_id),"wb") 
          
            index = 0
            for entity in self.entities:
                 vDict[entity] = projected_matrix[index]
                 vec_str = self.convert_to_str(vDict[entity])
                 index = index + 1          
               
            pickle.dump (vDict, reprDict)
              

            snapID_to_eval = end_snap_id  

            classeval = ""
            if os.environ['TASK'] == 'PRED': 
                classeval = ClassificationEvaluationLinkPrediction(isMerged=False) 
            else:
                classeval = ClassificationEvaluationLinkForecast(isMerged=False)
            classeval.generateData(snapID_to_eval, self.reprName,\
                 os.environ['COMPOSITION'])
            acc, auc, auprc, ndcg = classeval.runClassificationTask(snapID_to_eval,\
                 self.reprName)
            auc_list.append(auc)
            acc_list.append(acc)
            auprc_list.append(auprc)
            ndcg_list.append(ndcg)

        self.writeResults(result_file, result_trace_file,\
                    acc_list, auc_list, auprc_list, ndcg_list)
