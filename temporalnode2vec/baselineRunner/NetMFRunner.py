#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import pickle
import random 
import gensim
import scipy.io
import theano
import numpy as np
import networkx as nx 
from theano import tensor as T
import scipy.sparse as sparse
from scipy.sparse import csgraph
from gensim.models import Doc2Vec
from logManager.Logger import Logger
from word2vec.WordDoc2Vec import WordDoc2Vec
from baselineRunner.BaselineRunner import BaselineRunner 



label_node = lambda id_: 'V_%i' %(id_)


class NetMFRunner(BaselineRunner):
    def __init__(self, *args, **kwargs):
        self.reprName = "netmf"
        self.dataDir = os.environ['DATADIR']
        self.paramDict = kwargs['pDict']
        self.window_size = self.paramDict['window_size']
        if  'eig_pair' in self.paramDict:
            self.eigenpairs = self.paramDict['eig_pair']
        self.negtive_samples = 5

    def prepareData(self, graph_snapshot, snap_id):
        """
        """
        self.G = graph_snapshot
        self.entities = self.G.nodes()
        self.snapID = snap_id

    def load_adjacency_matrix(self, file, variable_name="network"):
        """
        """
        data = scipy.io.loadmat(file)
        Logger.logger.info("loading mat file %s", file)
        return data[variable_name]

    def deepwalk_filter(self, evals, window):
        """
        """
        for i in range(len(evals)):
            x = evals[i]
            evals[i] = 1. if x >= 1 else x*(1-x**window) / (1-x) / window
        evals = np.maximum(evals, 0)
        Logger.logr.info("After filtering, max eigenvalue=%f, min eigenvalue=%f",
                np.max(evals), np.min(evals))
        return evals

    def approximate_normalized_graph_laplacian(self, A, rank, which="LA"):
        """
        """
        n = A.shape[0]
        L, d_rt = csgraph.laplacian(A, normed=True, return_diag=True)
        # X = D^{-1/2} W D^{-1/2}
        X = sparse.identity(n) - L
        Logger.logr.info("Eigen decomposition...")
        #evals, evecs = sparse.linalg.eigsh(X, rank,
        #        which=which, tol=1e-3, maxiter=300)
        evals, evecs = sparse.linalg.eigsh(X, rank, which=which)
        Logger.logr.info("Maximum eigenvalue %f, minimum eigenvalue %f", np.max(evals), np.min(evals))
        Logger.logr.info("Computing D^{-1/2}U..")
        D_rt_inv = sparse.diags(d_rt ** -1)
        D_rt_invU = D_rt_inv.dot(evecs)
        return evals, D_rt_invU

    def approximate_deepwalk_matrix(self, evals, D_rt_invU, window, vol, b):
        evals = self.deepwalk_filter(evals, window=window)
        X = sparse.diags(np.sqrt(evals)).dot(D_rt_invU.T).T
        m = T.matrix()
        mmT = T.dot(m, m.T) * (vol/b)
        f = theano.function([m], T.log(T.maximum(mmT, 1))) #
        Y = f(X.astype(theano.config.floatX)) #
        Logger.logr.info("Computed DeepWalk matrix with %d non-zero elements",
                np.count_nonzero(Y))
        return sparse.csr_matrix(Y)

    def svd_deepwalk_matrix(self, X, dim):
        u, s, v = sparse.linalg.svds(X, dim, return_singular_vectors="u")
        # return U \Sigma^{1/2}
        return sparse.diags(np.sqrt(s)).dot(u.T).T


    def netmf_large(self, dimension):
        Logger.logr.info("Running NetMF for a large window size...")
        Logger.logr.info("Window size is set to be %d", self.window_size)


        # load adjacency matrix
        A = nx.to_scipy_sparse_matrix(self.G, nodelist=self.entities)
        vol = float(A.sum())


        # perform eigen-decomposition of D^{-1/2} A D^{-1/2}
        # keep top #rank eigenpairs
        evals, D_rt_invU = self.approximate_normalized_graph_laplacian(A, rank=self.eigenpairs, which="LA")

        # approximate deepwalk matrix
        deepwalk_matrix = self.approximate_deepwalk_matrix(evals, D_rt_invU,
                window=self.window_size,
                vol=vol, b=self.negtive_samples)

        # factorize deepwalk matrix with SVD
        deepwalk_embedding = self.svd_deepwalk_matrix(deepwalk_matrix, dim=dimension)
        return deepwalk_embedding



    def direct_compute_deepwalk_matrix(self, A, window, b):
        n = A.shape[0]
        vol = float(A.sum())
        L, d_rt = csgraph.laplacian(A, normed=True, return_diag=True)
        # X = D^{-1/2} A D^{-1/2}
        X = sparse.identity(n) - L
        S = np.zeros_like(X)
        X_power = sparse.identity(n)
        for i in range(window):
            Logger.logr.info("Compute matrix %d-th power", i+1)
            X_power = X_power.dot(X)
            S += X_power
        S *= vol / window / b
        D_rt_inv = sparse.diags(d_rt ** -1)
        M = D_rt_inv.dot(D_rt_inv.dot(S).T)
        #print (type(M))
        m = T.matrix()
        f = theano.function([m], T.log(T.maximum(m, 1)))
        Y = f(M.todense().astype(theano.config.floatX))
        return sparse.csr_matrix(Y)

    def netmf_small(self, dimension):
        Logger.logr.info("Running NetMF for a small window size...")
        Logger.logr.info("Window size is set to be %d", self.window_size)


        # load adjacency matrix
        A = nx.to_scipy_sparse_matrix(self.G, nodelist=self.entities)

        # directly compute deepwalk matrix
        deepwalk_matrix = self.direct_compute_deepwalk_matrix(A,
                window=self.window_size, b=self.negtive_samples)

        # factorize deepwalk matrix with SVD
        deepwalk_embedding = self.svd_deepwalk_matrix(deepwalk_matrix, dim=dimension)
      
        return deepwalk_embedding

    def runTheBaseline(self, latent_space_size):
        dataset_name = os.environ['DATASET']
        dirname = "%s/%s"%(self.dataDir,dataset_name)

        if  not (os.path.exists (dirname)): 
            os.makedirs(dirname)

        embedding = self.netmf_large(latent_space_size)
        reprDict = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID),"wb") 
        
        pos = 0
        vDict = {}
        for entity in self.entities:
            vDict[entity] = embedding[pos]
            pos = pos + 1
            
        pickle.dump (vDict, reprDict)