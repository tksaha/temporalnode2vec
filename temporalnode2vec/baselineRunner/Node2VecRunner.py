#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import numpy
import pickle
from sklearn import metrics
import networkx as nx 
import gensim 
from logManager.Logger import Logger 
from node2vec.Node2Vec import Node2Vec 
from baselineRunner.BaselineRunner import BaselineRunner

class Node2VecRunner(BaselineRunner):
    def __init__(self, *args, **kwargs):
        self.reprName = "node2vec"
        self.dataDir = os.environ['DATADIR']
        self.num_walk = int(os.environ['NUM_WALK'])
        self.walk_length = int(os.environ['WALK_LEN'])
        self.paramDict = kwargs['pDict']

    def prepareData(self, graph_snapshot, snap_id):
        """
        """
        self.G = graph_snapshot
        g = nx.Graph()

        for nodes in self.G.nodes():
            g.add_node(nodes)

        for edges in self.G.edges():
            g.add_edge(edges[0], edges[1], weight=1.0)

        self.G = g 

        self.entities = self.G.nodes()
        self.snapID = snap_id
        
    def runTheBaseline(self, latent_space_size):
        """
        """
        dataset_name = os.environ['DATASET']

        Logger.logr.info("Running Node2vec Internal")
        Logger.logr.info("Working a graph with %i edges"%self.G.number_of_edges())

        walkInputFileName = "%s/%s/%s_node2vecwalk_%i.txt"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID)
        node2vecInstance = Node2Vec (dimension=latent_space_size, window_size=10,\
             num_walks=self.num_walk,\
                 walk_length=self.walk_length+1,\
                     p=self.paramDict['p'], q=self.paramDict['q'])
        reprFile = "%s/%s/%s_repr_%i.txt"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID)

        generate_walk = True 
        if  generate_walk==True:
            Logger.logr.info("Generating Walk File")
            node2vecInstance.getWalkFile(self.G, walkInputFileName)
        node2vecInstance.learnEmbeddings(walkInputFileName, False, None, reprFile, retrofit=0, beta=0)

        vDict = {}
        vecModel = gensim.models.KeyedVectors.\
                    load_word2vec_format(reprFile, binary=False)
        reprDict = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID),"wb") 
        
        for entity in self.entities:
            vDict[entity] = vecModel[str(entity)]
            vec_str = self.convert_to_str(vDict[entity])

        pickle.dump (vDict, reprDict)
        
    def runEvaluationTask(self):
        """
        """
        pass