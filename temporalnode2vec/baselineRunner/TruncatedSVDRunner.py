#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import numpy
import pickle
import networkx as nx 
from sklearn.decomposition import TruncatedSVD
from baselineRunner.BaselineRunner import BaselineRunner


class TruncatedSVDRunner(BaselineRunner):
    def __init__(self, *args, **kwargs):
        self.reprName = "tsvd"
        self.dataDir = os.environ['DATADIR']
        self.entities = list()
        self.snapID = 1
        self.G = nx.Graph()

    def prepareData(self, graph_snapshot, snap_id):
        """
        """
        self.G = graph_snapshot
        self.entities = self.G.nodes()
        self.snapID = snap_id
    

    def runTheBaseline(self, latent_space_size):
        """
        """
        dataset_name = os.environ['DATASET']
        matrix = nx.adjacency_matrix(self.G, self.entities).todense()

        # Run PCA 
        vDict = {}
        
        svd = TruncatedSVD(n_components=latent_space_size,\
             n_iter=7, random_state=42)
        matrix_transformed = svd.fit_transform(matrix)

        
        reprFile = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID),"wb") 

        for entity in range(0, len(self.entities)):
            vec_ = matrix_transformed[entity]
            vDict[self.entities[entity]] = vec_ 
            

        pickle.dump(vDict, reprFile)    
        reprFile.close()

        
    def runEvaluationTask(self):
        """
        """
        pass 