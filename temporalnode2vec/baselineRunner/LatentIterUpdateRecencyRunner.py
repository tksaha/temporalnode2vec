import os 
import sys 
import pickle
import random 
import gensim
import importlib
import numpy as np
import networkx as nx 
from logManager.Logger import Logger 
from baselineRunner.BaselineRunner import BaselineRunner
from baselineRunner.DeepWalkRunner import DeepWalkRunner
from baselineRunner.IterUpdateRecencyVecReprRunner import ItUpdateRecencyVecRunner
from evaluation.classificationevaluation.ClassificationEvaluationLinkPrediction import ClassificationEvaluationLinkPrediction
from evaluation.classificationevaluation.ClassificationEvaluationLinkForecast import ClassificationEvaluationLinkForecast


module_dict = {"deepwalk": "baselineRunner.DeepWalkRunner",\
         "node2vec": "baselineRunner.Node2VecRunner",\
         "line" : "baselineRunner.LineRunner",\
         "tsvd":"baselineRunner.TruncatedSVDRunner",\
         "pca": "baselineRunner.PCARunner",\
         "spectral": "baselineRunner.SpectralClusteringRunner",\
         "modularity": "baselineRunner.ModularityMaximizationRunner",\
         "lle": "baselineRunner.LLERunner",\
         "isomap":"baselineRunner.IsoMapRunner",\
         "mds": "baselineRunner.MDS"}

class_dict = {"deepwalk":"DeepWalkRunner", "pca":"PCARunner",\
            "node2vec": "Node2VecRunner",\
            "line": "LineRunner",\
            "tsvd":"TruncatedSVDRunner",\
            "spectral": "SpectralClusteringRunner",\
            "modularity": "ModularityMaximizationRunner",\
            "lle": "LLERunner",\
            "isomap": "IsoMapRunner",\
            "mds": "MDSRunner" }


class LatentIterUpdateRecencyRunner(BaselineRunner):
    def __init__(self, *args, **kwargs):
        
        self.paramDict = kwargs['paramDict']
        method = self.paramDict['method']
        
        module = module_dict[method]
        klass  = class_dict[method]
        self.Klass = getattr(importlib.import_module(module), klass)
        self.dataDir = os.environ['DATADIR']

    def prepareData(self):
        pass 

    # Running (reprLearner) + ItUp combination
    def runTheBaseline(self):  
        nslots = int(os.environ['NSLOTS'])
        param_dict_list = self.get_param_dict_list()

        st_snap_id = int(os.environ['STSNAPID'])
        end_snap_id = int(os.environ['ENDSNAPID'])

        dataset_name = os.environ['DATASET']
        dirname = os.path.join(self.dataDir,dataset_name)
        latSpaceSize = int(os.environ['latSize'])

        result_file = open(os.environ['RES_FILE'], "a")
        param_file = open(os.environ['PARAM_FILE'], "a")

        optlist = []
        os.environ['TEST_MODE']='VALID'

        for pos in range(0, len(param_dict_list)):
            v2Vec = {}
            for snapID in range(st_snap_id, end_snap_id):
                if  snapID == st_snap_id:
                    runner = self.Klass(pDict=param_dict_list[pos])
                    self.reprName = "%s_%s"%(runner.reprName, "iterupdate_recency")
                    g = nx.read_gpickle(os.path.join(dirname, "graph_%i"%snapID))
                    runner.prepareData(g, snapID)
                    runner.runTheBaseline(latSpaceSize)
                else:
                    irunner = ItUpdateRecencyVecRunner (pdict=param_dict_list[pos])
                    runner = self.Klass (pDict=param_dict_list[pos])
                    g = nx.read_gpickle(os.path.join(dirname, "graph_%i"%snapID))
                    irunner.prepareData(g, snapID, v2Vec)
                    v2Vec = irunner.runTheBaseline(latSpaceSize,\
                         runner.reprName, self.reprName)

            snapID_to_eval = end_snap_id  
            classeval = ""

            if os.environ['TASK'] == 'PRED':
                classeval = ClassificationEvaluationLinkPrediction(isMerged=False) 
            else:
                classeval = ClassificationEvaluationLinkForecast(isMerged=False)

            classeval.generateData(snapID_to_eval, self.reprName,\
                 os.environ['COMPOSITION'])
            acc, auc, auprc, ndcg = classeval.runClassificationTask(snapID_to_eval,\
                 self.reprName)
            optlist.append ((param_dict_list[pos], ndcg))


        param_file.write ("%s%s"%(self.reprName, os.linesep))
        for pos in range(0, len(optlist)):
            param_file.write ("%s%s"%(str(optlist[pos][0]), os.linesep))
            param_file.write ("ndcg value:%s%s"%(str(optlist[pos][1]), os.linesep))


        pos,val = self.getOptPos(optlist)
        pdict = param_dict_list[pos]


        param_file.write("Optimal Param%s"%os.linesep)
        param_file.write("pdict=%s%s"%(str(pdict),os.linesep))


        os.environ['TEST_MODE']='TEST'
        

        acc_list, auc_list, auprc_list, ndcg_list = [], [], [], []

        for pos in range (0, int(os.environ['REPEAT'])):
            v2Vec = {}
            for snapID in range(st_snap_id, end_snap_id):
                if  snapID == st_snap_id:
                    pdict['random'] = random.Random(pos+1)
                    runner = self.Klass(pDict=pdict)
                    self.reprName = "%s_%s"%(runner.reprName, "iterupdate_recency")
                    g = nx.read_gpickle(os.path.join(dirname, "graph_%i"%snapID))
                    runner.prepareData(g, snapID)
                    runner.runTheBaseline(latSpaceSize)
                else:
                    irunner = ItUpdateRecencyVecRunner(pdict=pdict)
                    runner = self.Klass (pDict=pdict)
                    g = nx.read_gpickle(os.path.join(dirname, "graph_%i"%snapID))
                    irunner.prepareData(g, snapID, v2Vec)
                    v2Vec = irunner.runTheBaseline(latSpaceSize,\
                         runner.reprName, self.reprName)

            snapID_to_eval = end_snap_id  

            classeval = ""
            if os.environ['TASK'] == 'PRED': 
                classeval = ClassificationEvaluationLinkPrediction(isMerged=False) 
            else:
                classeval = ClassificationEvaluationLinkForecast(isMerged=False)
            classeval.generateData(snapID_to_eval, self.reprName,\
                 os.environ['COMPOSITION'])
            acc, auc, auprc, ndcg = classeval.runClassificationTask(snapID_to_eval,\
                 self.reprName)
            auc_list.append(auc)
            acc_list.append(acc)
            auprc_list.append(auprc)
            ndcg_list.append(ndcg)

        result_file.write("%s,%s,%s,%s,%s%s"%(self.reprName,\
             np.average(acc_list), np.average(auc_list),\
             np.average(auprc_list), np.average(ndcg_list), os.linesep))

    
    def runEvaluationTask(self):
        pass