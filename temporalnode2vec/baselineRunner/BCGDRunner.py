import os
import sys
import pickle
import networkx as nx
import numpy as np
from bcgd.BCGD import BCGD
from logManager.Logger import Logger
from baselineRunner.BaselineRunner import BaselineRunner 
from evaluation.classificationevaluation.ClassificationEvaluationLinkPrediction import ClassificationEvaluationLinkPrediction
from evaluation.classificationevaluation.ClassificationEvaluationLinkForecast import ClassificationEvaluationLinkForecast




class BCGDRunner(BaselineRunner):
    def __init__(self, *args, **kwargs):
        self.paramDict = kwargs['paramDict']
        self.dataDir = os.environ['DATADIR']
        self.reprName = "bcgd"

    def prepareData(self):
        pass

    def prepareBCGDFormatGraph(self, g, fileToWrite):

        fileToWrite = open(fileToWrite, "w")
        fileToWrite.write("%s%s"%(len(g.nodes()),os.linesep))

        for node in g.nodes():
            node_str = "%i,%i"%(node, len(g.neighbors(node)))
            for nbr in sorted(g.neighbors(node)):
                node_str = "%s:%i,1.0"%(node_str,nbr)
            fileToWrite.write("%s%s"%(node_str,os.linesep))

    def getVDict(self, fileToread, latSpaceSize):
        vDict = {}
        isfirstLine = True 
        #print (fileToread)
        for line in open(fileToread):
            if  isfirstLine:
                isfirstLine = False
            else:
                line_split = line.strip().split(",")
                node_id = int (line_split[0])
                latent_values = []
                for pos in range (0, latSpaceSize):
                    latent_values.append(0.0)

                for pos in range (2, len(line_split)):
                    index = int(line_split[pos-1].split(":")[1])
                    val = float(line_split[pos].split(":")[0])
                    latent_values[index] = val

                #print (node_id, len(latent_values))
                vDict[node_id] = np.array(latent_values, dtype='float32')

        return vDict

    # Maximum allowable digit 5
    def getSnapIDString (self, snapID):
        snap_id_str = str(snapID)
        zero_to_pad = 5 - len(snap_id_str)

        return_str = ""
        for i in range (0, zero_to_pad):
            return_str = return_str+"0"

        return return_str + snap_id_str
 
    def runTheBaseline(self):
        param_dict_list = self.get_param_dict_list()

        st_snap_id = int(os.environ['STSNAPID'])
        end_snap_id = int(os.environ['ENDSNAPID'])

        dataset_name = os.environ['DATASET']
        dirname = os.path.join(self.dataDir,dataset_name)
        latSpaceSize = int(os.environ['latSize'])

        result_file = open(os.environ['RES_FILE'], "a")
        param_file = open(os.environ['PARAM_FILE'], "a")
        result_trace_file = open(os.environ['RES_TRACE_FILE'], "a")

        optlist = []
        os.environ['TEST_MODE'] = 'VALID'

        bcgd_graphs_dir = os.path.join(dirname,\
                "bcgd_graphs")
        if  not(os.path.exists(bcgd_graphs_dir)):
            os.mkdir(bcgd_graphs_dir)

        
        for snapID in range(st_snap_id, end_snap_id):
            g = nx.read_gpickle(os.path.join(dirname,\
                         "graph_%i"%snapID))
            fileToWrite = os.path.join(bcgd_graphs_dir,\
                 "graph_%s.txt"%self.getSnapIDString(snapID))
            self.prepareBCGDFormatGraph(g, fileToWrite)
            
        Logger.logr.info ("Finish preparing the graph data")

        for pos in range(0, len(param_dict_list)):
            Logger.logr.info ("Working for param=%s"%str(param_dict_list[pos]))

            bcgdrunner = BCGD(param_dict_list[pos])
            paramDict = bcgdrunner.initBCGDParams()
            paramDict["c"] = str(latSpaceSize)
            paramDict["datadir"] = bcgd_graphs_dir
            paramDict["p"] = bcgd_graphs_dir
            args = bcgdrunner.buildArgListforBCGD(paramDict)
            self.runProcess(args)

            fileToread = os.path.join(dirname, "%sZmatrix%i"%\
                    (bcgd_graphs_dir,(end_snap_id - st_snap_id -1 )))

            vDict = self.getVDict(fileToread, latSpaceSize)
            reprDict = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                      dataset_name, self.reprName, end_snap_id-1),"wb") 
            pickle.dump (vDict, reprDict, protocol=pickle.HIGHEST_PROTOCOL)

            Logger.logr.info (reprDict)
            filename = "%s/%s/%s_repr_%i.p"%(self.dataDir,\
                      dataset_name, self.reprName, end_snap_id-1)
            Logger.logr.info (os.path.getsize(filename))


            snapID_to_eval = end_snap_id  
            classeval = ""

            if os.environ['TASK'] == 'PRED':
                classeval = ClassificationEvaluationLinkPrediction(isMerged=False) 
            else:
                classeval = ClassificationEvaluationLinkForecast(isMerged=False)

            classeval.generateData(snapID_to_eval, self.reprName,\
                 os.environ['COMPOSITION'])
            acc, auc, auprc, ndcg = classeval.runClassificationTask(snapID_to_eval,\
                 self.reprName)
            optlist.append ((param_dict_list[pos], ndcg))

        param_file.write ("%s%s"%(self.reprName, os.linesep))
        for pos in range(0, len(optlist)):
            param_file.write ("%s%s"%(str(optlist[pos][0]), os.linesep))
            param_file.write ("ndcg value:%s%s"%(str(optlist[pos][1]), os.linesep))


        pos,val = self.getOptPos(optlist)
        pdict = param_dict_list[pos]

        param_file.write("Optimal Param%s"%os.linesep)
        param_file.write("pdict=%s%s"%(str(pdict),os.linesep))

        os.environ['TEST_MODE']='TEST'

        acc_list, auc_list, auprc_list, ndcg_list = [], [], [], []

        for pos in range (0, int(os.environ['REPEAT'])):
            bcgdrunner = BCGD(pdict)
            paramDict = bcgdrunner.initBCGDParams()
            paramDict["c"] = str(latSpaceSize)
            paramDict["datadir"] = bcgd_graphs_dir
            paramDict["p"] = bcgd_graphs_dir
            args = bcgdrunner.buildArgListforBCGD(paramDict)
            self.runProcess(args)

            fileToread = os.path.join(dirname, "%sZmatrix%i"%\
                    (bcgd_graphs_dir,(end_snap_id - st_snap_id -1 )))

            vDict = self.getVDict(fileToread, latSpaceSize)
            reprDict = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                      dataset_name, self.reprName, end_snap_id-1),"wb") 
            pickle.dump (vDict, reprDict, protocol=pickle.HIGHEST_PROTOCOL)

            snapID_to_eval = end_snap_id  

            classeval = ""
            if os.environ['TASK'] == 'PRED': 
                classeval = ClassificationEvaluationLinkPrediction(isMerged=False) 
            else:
                classeval = ClassificationEvaluationLinkForecast(isMerged=False)
            classeval.generateData(snapID_to_eval, self.reprName,\
                 os.environ['COMPOSITION'])
            acc, auc, auprc, ndcg = classeval.runClassificationTask(snapID_to_eval,\
                 self.reprName)
            auc_list.append(auc)
            acc_list.append(acc)
            auprc_list.append(auprc)
            ndcg_list.append(ndcg)
        

        self.writeResults(result_file, result_trace_file,\
                    acc_list, auc_list, auprc_list, ndcg_list)