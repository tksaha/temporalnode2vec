#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import pickle
import networkx as nx 
from scipy.sparse.linalg import eigsh
from baselineRunner.BaselineRunner import BaselineRunner




class SpectralClusteringRunner(BaselineRunner):
    def __init__(self, *args, **kwargs):
        self.reprName = "spectral"
        self.dataDir = os.environ['DATADIR']
        self.entities = list()
        self.snapID = 1
        self.G = nx.Graph()

    def prepareData(self, graph_snapshot, snap_id):
        """
        """
        self.G = graph_snapshot
        self.entities = self.G.nodes()
        self.snapID = snap_id
        
    def runTheBaseline(self, latent_space_size):
        """
        """
        dataset_name = os.environ['DATASET']
        vDict = {}
        normalized_laplacian_matrix = nx.normalized_laplacian_matrix(self.G, self.entities)
        vals, reduced_vecs = eigsh(normalized_laplacian_matrix,\
             k=latent_space_size, which='SM')

        reprFile = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID),"wb") 

        for entity in range(0, len(self.entities)):
            vec_ = reduced_vecs[entity]
            vDict[self.entities[entity]] = vec_ 
            

        pickle.dump(vDict, reprFile)    
        reprFile.close()
        
    def runEvaluationTask(self):
        """
        """
        pass 