#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import pickle
import gensim 
import numpy as np
import networkx as nx 
from sklearn import metrics
from lineLinux.Line import Line 
from logManager.Logger import Logger 
from baselineRunner.BaselineRunner import BaselineRunner 

class LineRunner(BaselineRunner):
    def __init__(self, *args, **kwargs):
        self.reprName = "line"
        self.dataDir = os.environ['DATADIR']
        self.entities = list()
        self.G = nx.Graph()
        self.paramDict = kwargs['pDict']
        self.snapID = -1
        
    def prepareData(self, graph_snapshot, snap_id):
        """
        """
        self.G = graph_snapshot
        self.entities = self.G.nodes()
        self.snapID = snap_id

    def convertGraphInLineFormat(self, g):
        dataset_name = os.environ['DATASET']
        graph = "%s/%s/%s_graph_%i.txt"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID)

        graph_file = open(graph, "w")
        for edges in g.edges(): 
            graph_file.write("%i %i %i%s"%(edges[0], edges[1], 1, os.linesep))
            graph_file.write("%i %i %i%s"%(edges[1], edges[0], 1, os.linesep))

        return graph
        
    def runTheBaseline(self, latent_space_size):
        """
        """
        dataset_name = os.environ['DATASET']

        output_file = "%s/%s/%s_repr_%i_order1.txt"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID)

        lrunner = Line()
        lpDict = lrunner.initLineParams()
        lpDict["size"] = latent_space_size/2.0
        lpDict['output'] = output_file
        lpDict["samples"] = self.paramDict['iter']
        lpDict['train'] = self.convertGraphInLineFormat(self.G)
        

        args = lrunner.buildArgListforLine(lpDict)
        self.runProcess(args)

        
        vecModel = gensim.models.KeyedVectors.\
                    load_word2vec_format(output_file, binary=False)
        

        output_file = "%s/%s/%s_repr_%i_order2.txt"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID)
        lrunner = Line()
        lpDict = lrunner.initLineParams()
        lpDict["size"] = latent_space_size/2.0
        lpDict['order'] = 2
        lpDict['output'] = output_file
        lpDict['train'] = self.convertGraphInLineFormat(self.G)
        

        args = lrunner.buildArgListforLine(lpDict)
        self.runProcess(args)

        vecModel_order2 = gensim.models.KeyedVectors.\
                    load_word2vec_format(output_file, binary=False)


        vDict = {}
        reprDict = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID),"wb") 
        for entity in self.entities:
            try:
                vDict[entity] = np.hstack((vecModel[str(entity)], vecModel_order2[str(entity)]))
            except: 
                vDict[entity] = np.hstack((vecModel['rand'], vecModel['rand']))
           
        pickle.dump (vDict, reprDict)
        
    def runEvaluationTask(self):
        """
        """
        pass 