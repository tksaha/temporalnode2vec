#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import numpy
import pickle
import random 
import gensim
import networkx as nx 
from logManager.Logger import Logger 
from gensim.models import Doc2Vec
from word2vec.WordDoc2Vec import WordDoc2Vec
from baselineRunner.BaselineRunner import BaselineRunner 
from retrofitters.IterativeUpdateRecencyRetrofitter import IterativeUpdateRecencyRetrofitter

label_node = lambda id_: 'V_%i' %(id_)


class ItUpdateRecencyVecRunner(BaselineRunner):
    def __init__(self, *args, **kwargs):
        self.reprName = "itup"
        self.dataDir = os.environ['DATADIR']
        self.entities = list()
        self.G = nx.Graph()
        self.snapID = -1
        self.v2Vec = {}
        self.paramDict  = kwargs['pdict']
        
    def prepareData(self, graph_snapshot, snap_id, v2Vec):
        """
        """
        self.G = graph_snapshot
        self.entities = self.G.nodes()
        self.snapID = snap_id
        self.v2Vec = v2Vec

    def convert_to_str(self, vec):
        str_ = ""   
        for val in vec: 
            str_ ="%s %0.3f"%(str_,val)    
        return str_

    def runTheBaseline(self, latent_space_size, reprToLoad, repName):
        dataset_name = os.environ['DATASET']
        dirname = "%s/%s"%(self.dataDir,dataset_name)

        self.reprName = repName

        if  not (os.path.exists (dirname)): 
            os.makedirs(dirname)
        
        st_snap_id = int(os.environ['STSNAPID'])

        if self.snapID == st_snap_id:
            Logger.logr.info ("Not possible to update representation")
            sys.exit(1) 
        elif self.snapID == st_snap_id+1:
            file_to_load = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                 dataset_name, reprToLoad, st_snap_id),"rb")
            self.v2Vec =  pickle.load(file_to_load)

        retrofitter = IterativeUpdateRecencyRetrofitter(numIter=20,\
                 nx_Graph = self.G, pdict=self.paramDict) 
        v2Vec  = retrofitter.retrofitWithIterUpdate(self.v2Vec)
    

        reprFile = open("%s/%s/%s_repr_%i.p"%(self.dataDir,\
                 dataset_name, self.reprName, self.snapID),"wb") 
        pickle.dump(v2Vec, reprFile, protocol=pickle.HIGHEST_PROTOCOL)

        reprFile.flush()
        reprFile.close()
        return v2Vec
