#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import math 
from abc import ABCMeta, abstractmethod
from log_manager.log_config import Logger


class BaselineEvaluator:
    """
    BaselineEvaluator
    """
    def __init__(self, dbstring, **kwargs):
        self.window_size_list = ["8", "10", "12"]
        self.metric = {}
        self.metric_str = ""

    @abstractmethod
    def getOptimumParameters(self, optPDict, latent_space_size):
        pass 

    @abstractmethod
    def evaluateOptimum(self,  latent_space_size, optPDict):
        pass 


    '''
    '''
    def __writeResult(self, latreprName, f):
    	pass 
    
    def evaluate(self, baseline, prefix, latent_space_size):
        baseline.runTheBaseline(1, latent_space_size)
        baseline.runEvaluationTask()
        f1 = self.__getF1("%s%s"%(baseline.latReprName, prefix))
        return f1

    def writeResults(self, pd, rbase, latent_space_size, baseline, filePrefix, f):
        baseline.prepareData(pd)        
        baseline.runTheBaseline(rbase,latent_space_size)
        baseline.runEvaluationTask()
        self.__writeResult("%s%s"%(baseline.latReprName, filePrefix), f) 
        f.flush()