#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os 
import sys 
import scipy.io
import networkx as nx 
import  h5py

from logManager.Logger import Logger 



class MatLabMatrixDataReader:
	def __init__(self, *args, **kwargs): 
		self.matrixFile = kwargs['matfilename']

	def processDataAndGetAGraph(self, time):
		"""
		This function reads a 3 dimensional matrix from a 
		.mat file. The third dimension represents 
		the time dimension. Please note the differences 
		in the matrix read from different version 
		of .mat file. The earlier version reads as rXcXt whereas
		the latest version reads as tXcXr. The function also 
		convert the matrix into a networkx graph. 
		"""
		try: 
			matrix = scipy.io.loadmat(self.matrixFile)["adj"]
			nx_G = nx.from_numpy_matrix(matrix[:,:,time ])
			return nx_G

		except Exception as e:
			Logger.logr.info(" Using h5py to read mat v7.3 version file")
			matrix = h5py.File(self.matrixFile)["adj"]
			nx_G = nx.from_numpy_matrix(matrix[time, :, :])
			return nx_G