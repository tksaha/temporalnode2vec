import os
import sys
import numpy as np
from utility.ArgumentParserUtility  import ArgumentParserUtility


argparser = ArgumentParserUtility("Process Results")
argparser.addArgumentToParser("dataset", "Please enter dataset "\
        "to work on [enron, smsa, college, infection, fb,"\
        " hepph, dblp, dblp2, dblp3, nips, ytube, fb2 ]", True)
argparser.addArgumentToParser("lats", "Please enter latent space size",True)
argparser.addArgumentToParser("rrun", "Please enter number of random run",True)



argparser.parseArgument()

dataset = argparser.getValueOfAnArgument("dataset")
latSpaceSize = int(argparser.getValueOfAnArgument("lats"))
random_run = int(argparser.getValueOfAnArgument("rrun"))

os.environ['DATASET'] = '%s_%s'%(dataset,os.environ['TASK'])
#os.environ['DATASET'] = '%smsg'%(dataset)
#os.environ['DATASET'] = '%s'%(dataset)
dataDir = os.environ['DATADIR']




# Process Results
method_list = []
acc_list, auc_list, auprc_list, ndcg_list = [],[],[],[] 


for i in range (1, random_run+1):  
    os.environ['RND_SEED'] = str(1000 * i)
    result_file = open(os.path.join(dataDir, os.environ['DATASET'],\
                 "%s_combined_results_%i_%s.csv"\
                 %(os.environ['DATASET'],latSpaceSize,os.environ['RND_SEED'])))

    flip = 1
    for line in result_file:
        line_elems = line.strip().split(",")
        if  flip == 1:
            flip = 0
            line_elems = line.strip().split(",")

            acc = float(line_elems[1])
            auc = float(line_elems[2])
            auprc = float(line_elems[3])
            ndcg = float(line_elems[4])

            if  line_elems[0] not in method_list:
                method_list.append(line_elems[0])
                print (method_list)
                acc_list.append([acc])
                auc_list.append([auc])
                auprc_list.append([auprc])
                ndcg_list.append([ndcg])
            else:
                id_ = method_list.index(line_elems[0])
                acc_list[id_].append(acc)
                auc_list[id_].append(auc)
                auprc_list[id_].append(auprc)
                ndcg_list[id_].append(ndcg)

        else:
            flip = 1


assert (len(acc_list)   == len(method_list))
assert (len(auc_list)   == len(method_list))
assert (len(auprc_list) == len(method_list))
assert (len(ndcg_list)  == len(method_list))



acc_file = open(os.path.join(dataDir, os.environ['DATASET'],\
                     "%s_acc_%i.csv"\
                     %(os.environ['DATASET'],latSpaceSize)), "w")
auc_file = open(os.path.join(dataDir, os.environ['DATASET'],\
                 "%s_auc_%i.csv"\
                 %(os.environ['DATASET'],latSpaceSize)), "w") 
auprc_file = open(os.path.join(dataDir, os.environ['DATASET'],\
                 "%s_auprc_%i.csv"\
                 %(os.environ['DATASET'],latSpaceSize)), "w")
ndcg_file = open(os.path.join(dataDir, os.environ['DATASET'],\
                 "%s_ndcg_%i.csv"\
                 %(os.environ['DATASET'],latSpaceSize)), "w")
combined_file = open(os.path.join(dataDir, os.environ['DATASET'],\
                 "%s_combined_%i.csv"\
                 %(os.environ['DATASET'],latSpaceSize)), "w")


pos = 0

combined_file.write ("%s,%s,%s,%s,%s%s"%('method','acc','auc','auprc','ndcg',os.linesep))

for method in method_list:
    acc_file.write ("%s,%.4f,%0.4f%s"%(method,np.average(acc_list[pos]),\
             np.std(acc_list[pos]), os.linesep))
    auc_file.write ("%s,%.4f,%0.4f%s"%(method,np.average(auc_list[pos]),\
             np.std(auc_list[pos]), os.linesep))
    auprc_file.write ("%s,%.4f,%0.4f%s"%(method,np.average(auprc_list[pos]),\
             np.std(auprc_list[pos]), os.linesep))
    ndcg_file.write ("%s,%.4f,%0.4f%s"%(method,np.average(ndcg_list[pos]),\
             np.std(ndcg_list[pos]), os.linesep))
    
    combined_file.write ("%s,%.4f,%0.4f,%0.4f,%0.4f%s"%(method,np.average(acc_list[pos]),\
             np.average(auc_list[pos]), np.average(auprc_list[pos]), np.average(ndcg_list[pos]), os.linesep))
    combined_file.write ("%s,%.4f,%0.4f,%0.4f,%0.4f%s"%(method,np.std(acc_list[pos]),\
             np.std(auc_list[pos]), np.std(auprc_list[pos]), np.std(ndcg_list[pos]), os.linesep))

    #print (method)
    # assert(len(acc_list[pos]) == 20)
    # assert(len(auc_list[pos]) == 20)
    # assert(len(auprc_list[pos]) == 20)
    # assert(len(ndcg_list[pos]) == 20)
    # assert("=================================")

    pos = pos+1
