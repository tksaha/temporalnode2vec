#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
from abc import ABCMeta, abstractmethod
from logManager.Logger import Logger
from edgeFeatureGenerator.EdgeFeatBaseline import EdgeFeatBaseline


class HadamardFeatGen(EdgeFeatBaseline):
    def __init__(self, *args, **kwargs):
        self.vecDict = kwargs['vecDict']

    
    def getFeatData(self, n1, n2):
        """
        """
        return self.vecDict[n1] * self.vecDict[n2]