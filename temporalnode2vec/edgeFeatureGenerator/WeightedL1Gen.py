#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import math 
from abc import ABCMeta, abstractmethod
from logManager.Logger import Logger
from edgeFeatureGenerator.EdgeFeatBaseline import EdgeFeatBaseline


class WeightedL1FeatGen(EdgeFeatBaseline):
    def __init__(self, *args, **kwargs):
        self.vecDict = kwargs['vecDict']

    
    def getFeatData(self, n1, n2):
        """
        """
        return abs(self.vecDict[n1] - self.vecDict[n2])