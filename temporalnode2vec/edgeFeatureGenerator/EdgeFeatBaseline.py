#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
from abc import ABCMeta, abstractmethod
from logManager.Logger import Logger




class EdgeFeatBaseline:
    def __init__(self, *args, **kwargs):
        pass 

    @abstractmethod
    def getFeatData(self, n1, n2, vecDict):
        """
        """
        pass