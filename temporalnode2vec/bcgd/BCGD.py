#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys


class BCGD:
	"""
	https://github.com/linhongseba/Temporal-Network-Embedding
	"""
	def __init__(self, *args, **kwargs):
		self.bcgdParamDict = {}
		self.bcgdEXE = os.environ['BCGDEXEFILE']
		
	def initBCGDParams(self):
		self.bcgdParamDict["t"] = str(5)
		self.bcgdParamDict["c"] = str(64)
		self.bcgdParamDict["i"] = str(300)
		return self.bcgdParamDict

	def buildArgListforBCGD(self, bcgddict):
		args = [self.bcgdEXE, bcgddict["datadir"],\
				"-t", self.bcgdParamDict['t'],\
				"-c", self.bcgdParamDict['c'],\
				"-p", self.bcgdParamDict['p'],\
				"-i", self.bcgdParamDict['i']]
		return args




