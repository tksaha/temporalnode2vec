#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys 
import numpy
import random 
import datetime
import pandas as pd 
import networkx as nx 
from logManager.Logger import Logger 
from dataReader.DataReader import DataReader



class EnronReader(DataReader):
    def __init__(self, *args, **kwargs):
        self.fileName = os.environ["ENRONFILE"]
        self.startTime = int(os.environ['ENRONSTTIME'])
        self.endTime = int(os.environ["ENRONENDTIME"])
        os.environ['DATASET'] = 'enron_%s'%os.environ['TASK']
        self.dataFile = None 
        self.dataDir = os.environ['DATADIR']
        self.timeSlotsToSplit = (self.endTime  - self.startTime + 1 )
        os.environ['NSLOTS'] = str(self.timeSlotsToSplit)
        os.environ['STSNAPID'] = str(147)
        os.environ['ENDSNAPID'] = str(self.endTime)
        self.latSpaceSize = 0

    def processData(self, pd):
        if  pd == 1:
            Logger.logr.info("Starting Processing Data")
            self._processDataMatrixForm()

    def runSmoothedEmbeddingGenerator(self, latSpaceSize):
        self._runEmbeddingGenerator(latSpaceSize)
        
    def runEvaluation(self):
        pass