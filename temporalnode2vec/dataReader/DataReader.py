#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import datetime
import pandas as pd 
import numpy as np
import networkx as nx 
from logManager.Logger import Logger 
from abc import ABCMeta, abstractmethod
from processData.processData import MatLabMatrixDataReader
from baselineRunner.LatentIterUpdateRunner import LatentIterUpdateRunner
from baselineRunner.LatentIterUpdateRecencyRunner import LatentIterUpdateRecencyRunner
from baselineRunner.BCGDRunner import BCGDRunner
from baselineRunner.LatentSinglyRunner import LatentSinglyRunner
from baselineRunner.LatentMergedRunner import LatentMergedRunner
from evaluation.classificationevaluation.ClassificationEvaluation import ClassificationEvaluation
from baselineRunner.HomogeneousLinearProjectionRunner import HomogeneousLinearProjectionRunner
from baselineRunner.HeterogeneousLinearProjectionRunner import HeterogeneousLinearProjectionRunner
from baselineRunner.GrNMFRunner import GrNMFRunner
from baselineRunner.HomoNonLinearProjectionRunner import HomoNonLinearProjectionRunner
from baselineRunner.HeteroNonLinearProjectionRunner import HeteroNonLinearProjectionRunner



class DataReader:
    def __init__(self, *args, **kwargs):
        self.edgeDict = {}

    @abstractmethod
    def processData(self):
        pass

    def getGraphsAtTimes(self, dataFile, dateIndex,\
        indexfNodeID, indexlNodeID, timeSlotsToSplit):

        dirname = self.setupFolder()
        mergedGraph = nx.Graph ()
        nodes_has_edges_in_all_snapshot = []
        self.st_snap_id = int(os.environ['STSNAPID'])
        self.end_snap_id = int(os.environ['ENDSNAPID'])
        snapID = self.st_snap_id
        node_time_stamp_edges = []

        totlen = dataFile.shape[0]
        uTime = list(sorted(dataFile[dateIndex].unique()))
        totULen = len(uTime)

        startTime = uTime[0]
        len_of_all_frames  = 0

        all_nodes = []
        all_nodes.extend (self.dataFile[0])
        all_nodes.extend (self.dataFile[1])
        all_nodes = list(set(all_nodes))


        for time in range(0, timeSlotsToSplit):
            Logger.logr.info("Reading file from snapshot %i", time+1)
            if time == self.timeSlotsToSplit - 1:
               endPos = totULen - 1
               endTime = uTime[endPos]
               mask = (dataFile[dateIndex] >= startTime)\
                     & (dataFile[dateIndex] <= endTime)
            else:
               endPos = (int(totULen/timeSlotsToSplit) * (time + 1))
               endTime = uTime[endPos]
               mask = (dataFile[dateIndex] >= startTime)\
                     & (dataFile[dateIndex] < endTime)

            subFrame = dataFile.loc[mask]
            len_of_all_frames  = len_of_all_frames + len(subFrame)

            Logger.logr.info("Start Time = %s"%str(startTime))
            g = nx.from_pandas_dataframe (subFrame,\
                 indexfNodeID, indexlNodeID)
            nx_G = nx.Graph(g) 
            for node in all_nodes:
                if  not(nx_G.has_node(node)):
                    nx_G.add_node(node)

            nx_G.remove_edges_from(nx_G.selfloop_edges())
            startTime = endTime

            if  os.environ['TASK'] == 'PRED':
                if  time > self.end_snap_id -1:
                    self.writeTheEdgeIDListPrediction(nx_G, snapID, False)
            else:
                if  time > self.end_snap_id -2:
                    self.writeTheEdgeIDListForecast(nx_G, snapID, False)


            Logger.logr.info ("End Time = %s"%str(endTime))
            Logger.logr.info ("Total rows retrieved =%i"%len_of_all_frames)

            
            for pos in range(0, len(all_nodes)):
                if  time == self.st_snap_id:
                    try:
                        node_time_stamp_edges.append([len(nx_G.neighbors(all_nodes[pos]))])
                    except:
                        node_time_stamp_edges.append([0])
                else:
                    try:
                        node_time_stamp_edges[pos].append(len(nx_G.neighbors(all_nodes[pos])))
                    except:
                        node_time_stamp_edges[pos].append(0)

            for node in nx_G.nodes():
                mergedGraph.add_node(node)
                try: 
                    if  len(nx_G.neighbors(node)) == 0: 
                        try:
                            nodes_has_edges_in_all_snapshot.remove(node)
                        except:
                            pass 
                except:
                    pass 

            Logger.logr.info("Number of Nodes that have edges "\
             "in all shapshots = %i"%len(nodes_has_edges_in_all_snapshot))

            for edges in nx_G.edges():
                mergedGraph.add_edge(edges[0], edges[1])

            if  time == self.end_snap_id -1:
                if  os.environ['TASK'] == 'FORE':
                    self.writeTheEdgeIDListForecast(mergedGraph, snapID, True)


            # write both the graph and the merged graph
            if time == self.end_snap_id -1:
                merged_graph_file = os.path.join(dirname, "merged_graph_%i"%snapID)
                nx.write_gpickle(mergedGraph, merged_graph_file)

            
            graph_file = os.path.join(dirname, "graph_%i"%snapID)
            nx.write_gpickle(nx_G, graph_file)

            snapID = snapID + 1
            

        if  len_of_all_frames == totlen: 
            Logger.logr.info ("All rows has been successfully retrieved")

        # print node time stamp information
        node_time_stamp_edges_file = open(os.path.join(dirname, "time_stamps.txt"), "w")
        for pos in range(0, len(node_time_stamp_edges)):
            node_time_stamp_edges_file.write("%i  :"%all_nodes[pos])
            for nEd in node_time_stamp_edges[pos]:
                node_time_stamp_edges_file.write("  %i "%nEd)
            node_time_stamp_edges_file.write("%s"%os.linesep)    

        Logger.logr.info("Number of total nodes = %i" %len(all_nodes))
        Logger.logr.info("Number of Nodes that have edges "\
             "in start snapshot to end shapshot = %i"%\
              len(nodes_has_edges_in_all_snapshot))


    def _processDataMatrixForm(self):
        all_nodes = []     
        dirname = self.setupFolder()

        mergedGraph = nx.Graph ()
        matrixReader = MatLabMatrixDataReader(matfilename=self.fileName)

        nodes_has_edges_in_all_snapshot = []

        self.st_snap_id = int(os.environ['STSNAPID'])
        self.end_snap_id = int(os.environ['ENDSNAPID'])
        snapID = self.st_snap_id

        dirname = self.setupFolder()
        node_time_stamp_edges = []

        for time in range(self.st_snap_id, self.end_snap_id+1):
            Logger.logr.info("Reading file from snapshot %i", time)
            nx_G = matrixReader.processDataAndGetAGraph(time)

            if  time == self.st_snap_id:
                nodes_has_edges_in_all_snapshot = nx_G.nodes()
                Logger.logr.info ("Total nodes ====> %i"%len(nodes_has_edges_in_all_snapshot))


            if  os.environ['TASK'] == 'PRED':
                if  time > self.end_snap_id -1:
                    self.writeTheEdgeIDListPrediction(nx_G, snapID, False)
            else:
                if  time > self.end_snap_id -2:
                    self.writeTheEdgeIDListForecast(nx_G, snapID, False)

            if  time == self.st_snap_id:
                all_nodes.extend(nx_G.nodes())
            
            for pos in range(0, len(all_nodes)):
                if  time == self.st_snap_id:
                    node_time_stamp_edges.append([len(nx_G.neighbors(all_nodes[pos]))])
                else:
                    node_time_stamp_edges[pos].append(len(nx_G.neighbors(all_nodes[pos])))

            for node in nx_G.nodes():
                mergedGraph.add_node(node)
                if  len(nx_G.neighbors(node)) == 0: 
                    try:
                        nodes_has_edges_in_all_snapshot.remove(node)
                    except:
                        pass 

            Logger.logr.info("Number of Nodes that have edges "\
             "in all shapshots = %i"%len(nodes_has_edges_in_all_snapshot))

            for edges in nx_G.edges():
                mergedGraph.add_edge(edges[0], edges[1])

            if  time == self.end_snap_id -1:
                if  os.environ['TASK'] == 'FORE':
                    self.writeTheEdgeIDListForecast(mergedGraph, snapID, True)


            # write both the graph and the merged graph
            if time == self.end_snap_id -1:
                merged_graph_file = os.path.join(dirname, "merged_graph_%i"%snapID)
                nx.write_gpickle(mergedGraph, merged_graph_file)

            
            graph_file = os.path.join(dirname, "graph_%i"%snapID)
            nx.write_gpickle(nx_G, graph_file)

            snapID = snapID + 1

        # print node time stamp information
        node_time_stamp_edges_file = open(os.path.join(dirname, "time_stamps.txt"), "w")
        for pos in range(0, len(node_time_stamp_edges)):
            node_time_stamp_edges_file.write("%i  :"%all_nodes[pos])
            for nEd in node_time_stamp_edges[pos]:
                node_time_stamp_edges_file.write("  %i "%nEd)
            node_time_stamp_edges_file.write("%s"%os.linesep)    

        Logger.logr.info("Number of total nodes = %i" %len(all_nodes))
        Logger.logr.info("Number of Nodes that have edges "\
             "in start snapshot to end shapshot = %i"%\
              len(nodes_has_edges_in_all_snapshot))

    def _runEmbeddingGenerator(self, latSpaceSize):
        os.environ['RES_FILE'] =  os.path.join(self.dataDir, os.environ['DATASET'],\
                     "%s_combined_results_%i_%s.csv"\
                     %(os.environ['DATASET'],latSpaceSize,os.environ['RND_SEED']))
        os.environ['RES_TRACE_FILE'] =  os.path.join(self.dataDir,\
                     os.environ['DATASET'],\
                     "%s_combined_results_trace_%i_%s.csv"\
                     %(os.environ['DATASET'],latSpaceSize, os.environ['RND_SEED']))
        os.environ['PARAM_FILE'] =  os.path.join(self.dataDir, os.environ['DATASET'],\
                     "%s_combined_param_%i_%s.txt"\
                     %(os.environ['DATASET'],latSpaceSize, os.environ['RND_SEED']))

        result_file = open (os.environ['RES_FILE'], "w")
        result_trace_file = open (os.environ['RES_TRACE_FILE'], "w")
        param_file = open (os.environ['PARAM_FILE'], "w")
        result_file.close()
        param_file.close()

        # Run deepwalk iterupdate
        os.environ['latSize'] = str(latSpaceSize)



        
################ BCGD Runners #######################
        paramDict = {}
        paramDict ['dummy']= [8] 
        bcgdrunner = BCGDRunner(paramDict=paramDict)
        bcgdrunner.runTheBaseline()

################ GrNMF BaselineRunner ###################
        # paramDict = {}
        # paramDict ['theta']= [0.5] 
        # paramDict ['alpha']= [0.002] 
        # grnmfrunner = GrNMFRunner (paramDict=paramDict)
        # grnmfrunner.runTheBaseline()
        


################ Homogeneous   Runners #########################################################

        # paramDict = {}
        # paramDict ['method'] = 'netmf'
        # paramDict ['window_size'] = [8, 10]
        # paramDict ['eig_pair'] = [256, 256]
        # homorunner = HomogeneousLinearProjectionRunner(paramDict=paramDict)
        # homorunner.runTheBaseline()
        
        # os.environ['NLIN'] = 'relu'
        # paramDict = {}
        # paramDict ['method'] = 'deepwalk'
        # paramDict ['window_size'] = [8, 10, 15]
        # homorunner = HomoNonLinearProjectionRunner(paramDict=paramDict)
        # homorunner.runTheBaseline()

        # os.environ['NLIN'] = 'sigmoid'
        # paramDict = {}
        # paramDict ['method'] = 'deepwalk'
        # paramDict ['window_size'] = [8, 10, 15]
        # homorunner = HomoNonLinearProjectionRunner(paramDict=paramDict)
        # homorunner.runTheBaseline()

        # os.environ['NLIN'] = 'tanh'
        # paramDict = {}
        # paramDict ['method'] = 'deepwalk'
        # paramDict ['window_size'] = [8, 10, 15]
        # homorunner = HomoNonLinearProjectionRunner(paramDict=paramDict)
        # homorunner.runTheBaseline()


        # os.environ['HETER_COMB'] = 'wct'
        # os.environ['NLIN'] = 'relu'

        # paramDict = {}
        # paramDict ['method'] = 'deepwalk'
        # paramDict ['window_size'] = [8, 8, 8, 10,10,10, 15,15,15]
        # paramDict ['theta']=  [0.2,0.5, 0.7,0.2,0.5,0.7,0.2,0.5,0.7]
        # heterorunner = HeteroNonLinearProjectionRunner(paramDict=paramDict)
        # heterorunner.runTheBaseline()


        # os.environ['HETER_COMB'] = 'wct'
        # os.environ['NLIN'] = 'sigmoid'

        # paramDict = {}
        # paramDict ['method'] = 'deepwalk'
        # paramDict ['window_size'] = [8, 8, 8, 10,10,10, 15,15,15]
        # paramDict ['theta']=  [0.2,0.5, 0.7,0.2,0.5,0.7,0.2,0.5,0.7]
        # heterorunner = HeteroNonLinearProjectionRunner(paramDict=paramDict)
        # heterorunner.runTheBaseline()

        # os.environ['HETER_COMB'] = 'wct'
        # os.environ['NLIN'] = 'tanh'

        # paramDict = {}
        # paramDict ['method'] = 'deepwalk'
        # paramDict ['window_size'] = [8, 8, 8, 10,10,10, 15,15,15]
        # paramDict ['theta']=  [0.2,0.5, 0.7,0.2,0.5,0.7,0.2,0.5,0.7]
        # heterorunner = HeteroNonLinearProjectionRunner(paramDict=paramDict)
        # heterorunner.runTheBaseline()




       


        paramDict = {}
        paramDict ['method'] = 'deepwalk'
        paramDict ['window_size'] = [8, 10, 15]
        homorunner = HomogeneousLinearProjectionRunner(paramDict=paramDict)
        homorunner.runTheBaseline()

        
        paramDict = {}
        paramDict ['method'] = 'line'
        paramDict ['iter'] = [5,10,20]
        homorunner = HomogeneousLinearProjectionRunner(paramDict=paramDict)
        homorunner.runTheBaseline()

        paramDict = {}
        paramDict ['method'] = 'node2vec'
        paramDict ['p'] = [0.1, 0.3, 0.5]
        paramDict ['q'] = [0.5, 0.3, 0.1]
        homorunner = HomogeneousLinearProjectionRunner(paramDict=paramDict)
        homorunner.runTheBaseline()


        paramDict = {}
        paramDict ['method'] = 'pca'
        paramDict ['dummy']= [8] 
        homorunner = HomogeneousLinearProjectionRunner(paramDict=paramDict)
        homorunner.runTheBaseline()

        paramDict = {}
        paramDict ['method'] = 'tsvd'
        paramDict ['dummy']= [8] 
        homorunner = HomogeneousLinearProjectionRunner(paramDict=paramDict)
        homorunner.runTheBaseline()


        paramDict = {}
        paramDict ['method'] = 'lle'
        paramDict ['dummy']= [8]
        homorunner = HomogeneousLinearProjectionRunner(paramDict=paramDict)
        homorunner.runTheBaseline()

        
        paramDict = {}
        paramDict ['method'] = 'isomap'
        paramDict ['dummy']= [8]
        homorunner = HomogeneousLinearProjectionRunner(paramDict=paramDict)
        homorunner.runTheBaseline()

# ################ Heterogeneous Runners ########################################################
        os.environ['HETER_COMB'] = 'wct'


        paramDict = {}
        paramDict ['method'] = 'deepwalk'
        paramDict ['window_size'] = [8, 8, 8, 10,10,10, 15,15,15]
        paramDict ['theta']=  [0.2,0.5, 0.7,0.2,0.5,0.7,0.2,0.5,0.7]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()


        paramDict = {}
        paramDict ['method'] = 'line'
        paramDict ['iter'] = [5,10,20, 5, 10, 20, 5, 10, 20]
        paramDict ['theta'] = [0.2,0.5, 0.7,0.2,0.5,0.7,0.2,0.5,0.7]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()

        paramDict = {}
        paramDict ['method'] = 'node2vec'
        paramDict ['p'] = [0.1, 0.1, 0.1, 0.3, 0.3, 0.3, 0.5, 0.5, 0.5]
        paramDict ['q'] = [0.5, 0.5, 0.5, 0.3, 0.3, 0.3, 0.1, 0.1, 0.1]
        paramDict ['theta'] = [0.2,0.5, 0.7,0.2,0.5,0.7,0.2,0.5,0.7]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()


        paramDict = {}
        paramDict ['method'] = 'pca'
        paramDict ['theta']= [0.2, 0.5, 0.7]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()

        paramDict = {}
        paramDict ['method'] = 'tsvd'
        paramDict ['theta']= [0.2, 0.5, 0.7]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()


        paramDict = {}
        paramDict ['method'] = 'lle'
        paramDict ['theta']= [0.2, 0.5, 0.7]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()

        
        paramDict = {}
        paramDict ['method'] = 'isomap'
        paramDict ['theta']= [0.2, 0.5, 0.7]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()





# ################ Heterogeneous Runners ########################################################
        os.environ['HETER_COMB'] = 'avg'


        paramDict = {}
        paramDict ['method'] = 'deepwalk'
        paramDict ['window_size'] = [8, 10, 15]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()


        paramDict = {}
        paramDict ['method'] = 'line'
        paramDict ['iter'] = [5,10,20]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()

        paramDict = {}
        paramDict ['method'] = 'node2vec'
        paramDict ['p'] = [0.1, 0.3, 0.5]
        paramDict ['q'] = [0.5, 0.3, 0.1]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()


        paramDict = {}
        paramDict ['method'] = 'pca'
        paramDict ['dummy']= [8] 
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()

        paramDict = {}
        paramDict ['method'] = 'tsvd'
        paramDict ['dummy']= [8] 
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()


        paramDict = {}
        paramDict ['method'] = 'lle'
        paramDict ['dummy']= [8]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()

        
        paramDict = {}
        paramDict ['method'] = 'isomap'
        paramDict ['dummy']= [8]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()


# ################ Heterogeneous Runners ########################################################
        os.environ['HETER_COMB'] = 'linear'


        paramDict = {}
        paramDict ['method'] = 'deepwalk'
        paramDict ['window_size'] = [8, 10, 15]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()


        paramDict = {}
        paramDict ['method'] = 'line'
        paramDict ['iter'] = [5,10,20]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()

        paramDict = {}
        paramDict ['method'] = 'node2vec'
        paramDict ['p'] = [0.1, 0.3, 0.5]
        paramDict ['q'] = [0.5, 0.3, 0.1]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()


        paramDict = {}
        paramDict ['method'] = 'pca'
        paramDict ['dummy']= [8] 
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()

        paramDict = {}
        paramDict ['method'] = 'tsvd'
        paramDict ['dummy']= [8] 
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()


        paramDict = {}
        paramDict ['method'] = 'lle'
        paramDict ['dummy']= [8]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()

        
        paramDict = {}
        paramDict ['method'] = 'isomap'
        paramDict ['dummy']= [8]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()


# ################ Heterogeneous Runners ########################################################
        os.environ['HETER_COMB'] = 'exp'


        paramDict = {}
        paramDict ['method'] = 'deepwalk'
        paramDict ['window_size'] = [8, 10, 15]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()


        paramDict = {}
        paramDict ['method'] = 'line'
        paramDict ['iter'] = [5,10,20]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()

        paramDict = {}
        paramDict ['method'] = 'node2vec'
        paramDict ['p'] = [0.1, 0.3, 0.5]
        paramDict ['q'] = [0.5, 0.3, 0.1]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()


        paramDict = {}
        paramDict ['method'] = 'pca'
        paramDict ['dummy']= [8] 
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()

        paramDict = {}
        paramDict ['method'] = 'tsvd'
        paramDict ['dummy']= [8] 
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()


        paramDict = {}
        paramDict ['method'] = 'lle'
        paramDict ['dummy']= [8]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()

        
        paramDict = {}
        paramDict ['method'] = 'isomap'
        paramDict ['dummy']= [8]
        heterorunner = HeterogeneousLinearProjectionRunner(paramDict=paramDict)
        heterorunner.runTheBaseline()
       
############### Iterupdate Runners ###########################################################
        
        paramDict = {}
        paramDict ['method'] = 'deepwalk'
        paramDict ['window_size'] = [8, 8, 8, 8, 8,\
             10, 10, 10, 10, 10, 15,\
                 15, 15, 15, 15]
        paramDict ['alpha_v'] = [0.001, 0.01, 1, 5,\
         10, 0.001, 0.01, 1, 5, 10,\
          0.001, 0.01, 1, 5, 10]
        iterrunner = LatentIterUpdateRunner(paramDict=paramDict)
        iterrunner.runTheBaseline()

        
        paramDict = {}
        paramDict ['method'] = 'line'
        paramDict ['iter'] = [5,5,5,5,5,\
                    10,10,10,10,10,\
                    20,20,20,20,20]
        paramDict ['alpha_v'] = [0.001, 0.01, 1, 5, 10,\
                     0.001, 0.01, 1, 5, 10,\
                     0.001, 0.01, 1, 5, 10]
        iterrunner = LatentIterUpdateRunner(paramDict=paramDict)
        iterrunner.runTheBaseline()

        
        paramDict = {}
        paramDict ['method'] = 'node2vec'
        paramDict ['p'] = [0.1, 0.3, 0.5]
        paramDict ['q'] = [0.5, 0.3, 0.1]
        iterrunner = LatentIterUpdateRunner(paramDict=paramDict)
        iterrunner.runTheBaseline()



        paramDict = {}
        paramDict ['method'] = 'pca'
        paramDict ['alpha_v'] = [0.001, 0.01, 1, 5, 10]
        iterrunner = LatentIterUpdateRunner(paramDict=paramDict)
        iterrunner.runTheBaseline()


        paramDict = {}
        paramDict ['method'] = 'tsvd'
        paramDict ['alpha_v'] = [0.001, 0.01, 1, 5, 10]
        iterrunner = LatentIterUpdateRunner(paramDict=paramDict)
        iterrunner.runTheBaseline()


        paramDict = {}
        paramDict ['method'] = 'lle'
        paramDict ['alpha_v'] = [0.001, 0.01, 1, 5, 10]
        iterrunner = LatentIterUpdateRunner(paramDict=paramDict)
        iterrunner.runTheBaseline()

        
        paramDict = {}
        paramDict ['method'] = 'isomap'
        paramDict ['alpha_v'] = [0.001, 0.01, 1, 5, 10]
        iterrunner = LatentIterUpdateRunner(paramDict=paramDict)
        iterrunner.runTheBaseline()





    # Write the edge id list of every snapshot (each snapshot has train, 
    # test, and valid split)

    def setupFolder(self):
        dataset_name = os.environ['DATASET']
        dirname = os.path.join(self.dataDir,dataset_name)
        if  not(os.path.exists(dirname)):
            os.mkdir(dirname)
        return dirname

    def writeTheEdgeIDListPrediction(self, g, snapID, isMerged):
        dirname = self.setupFolder()
        np.random.seed(int(os.environ['RND_SEED']))

        id_label_train_filename, id_label_test_filename, id_label_valid_filename = "", "", ""

        if  isMerged == True:
            id_label_train_filename = os.path.join(dirname,\
                        "nidlabel_%i_merged_train.txt"%(snapID))
            id_label_test_filename = os.path.join(dirname,\
                        "nidlabel_%i_merged_test.txt"%(snapID))
            id_label_valid_filename = os.path.join(dirname,\
                        "nidlabel_%i_merged_valid.txt"%(snapID)) 
        else:
            id_label_train_filename = os.path.join(dirname,\
                    "nidlabel_%i_train.txt"%(snapID))
            id_label_test_filename = os.path.join(dirname,\
                    "nidlabel_%i_test.txt"%(snapID))
            id_label_valid_filename = os.path.join(dirname,\
                    "nidlabel_%i_valid.txt"%(snapID))

        id_label_train_file = open(id_label_train_filename, "w")
        id_label_valid_file = open(id_label_valid_filename, "w")
        id_label_test_file = open(id_label_test_filename, "w")

        dataG = nx.Graph()
        #Sample positive edges
        nedges = len(g.edges())
        shuffled_list = np.arange(nedges)
        np.random.shuffle(shuffled_list)

        nedges= min(50000, len(shuffled_list))
        shuffled_list = shuffled_list[0:nedges]

        Logger.logr.info ("Generating %i positive edges "%len(shuffled_list))


        edgelist = g.edges()
        ntrainids, ntestids, nvalidids = 0, 0, 0
        last_train_id = int(len(shuffled_list)*0.50)


        for pos in range(0, last_train_id):
            pos_ = shuffled_list[pos]
            id_label_train_file.write("%s,%s,pos"%(edgelist[pos_][0],edgelist[pos_][1]))
            id_label_train_file.write(os.linesep)
            dataG.add_edge(edgelist[pos_][0],edgelist[pos_][1])
            ntrainids = ntrainids + 1

        last_test_id = last_train_id + int(len(shuffled_list)*0.30)
        for pos in range(last_train_id, last_test_id):
            pos_ = shuffled_list[pos]
            id_label_test_file.write("%s,%s,pos"%(edgelist[pos_][0],edgelist[pos_][1]))
            id_label_test_file.write(os.linesep)
            dataG.add_edge(edgelist[pos_][0],edgelist[pos_][1])
            ntestids = ntestids + 1

        for pos in range (last_test_id, len(shuffled_list)):
            pos_ = shuffled_list[pos]
            id_label_valid_file.write("%s,%s,pos"%(edgelist[pos_][0],edgelist[pos_][1]))
            id_label_valid_file.write(os.linesep)
            dataG.add_edge(edgelist[pos_][0],edgelist[pos_][1])
            nvalidids = nvalidids + 1
 
        #print (nedges, ntrainids, ntestids, nvalidids)
        #print (len(dataG.edges()))

        Logger.logr.info ("Generating %i negative edges "%nedges)
        train_ids = 0
        test_ids = 0
        valid_ids = 0 

        nodelist = g.nodes()
        while True:
            # Select a node uniformly at random 
            n1 = np.random.choice(len(g.nodes()), 1, replace=True)[0]
            # Select another node uniformly at random
            n2 = np.random.choice(len(g.nodes()), 1, replace=True)[0]
            # Check if the pair exists already, if not dump it into the file
        
            n1 = nodelist [n1] 
            n2 = nodelist [n2]
            
            if  not(g.has_edge(n1, n2)) and n1!=n2 and\
                     not(dataG.has_edge(n1, n2)):
                if train_ids < ntrainids:
                    id_label_train_file.write("%s,%s,neg"%(n1,n2))
                    id_label_train_file.write(os.linesep)
                    train_ids = train_ids + 1
                elif test_ids < ntestids:
                    id_label_test_file.write("%s,%s,neg"%(n1,n2))
                    id_label_test_file.write(os.linesep)
                    test_ids = test_ids + 1
                else:
                    id_label_valid_file.write("%s,%s,neg"%(n1,n2))
                    id_label_valid_file.write(os.linesep)
                dataG.add_edge (n1, n2)
                
            if  len(dataG.edges()) == 2*nedges:
                break
        #print (len(dataG.edges()))        

    # Write the edge id list of every snapshot
    def writeTheEdgeIDListForecast(self, g, snapID, isMerged):
        dirname=self.setupFolder()
        np.random.seed(int(os.environ['RND_SEED']))


        npercent = 0.80
        if  snapID == self.end_snap_id -1:
            npercent = 1.00

        id_label_filename, id_label_valid_filename = "", ""


        if  isMerged == True:
            id_label_filename = os.path.join(dirname,\
                     "nidlabel_%i_merged.txt"%(snapID))
            if  npercent < 1.00:
                id_label_valid_filename = os.path.join(dirname,\
                    "nidlabel_%i_merged_valid.txt"%(snapID))
        else:
            id_label_filename = os.path.join(dirname,\
                    "nidlabel_%i.txt"%(snapID))
            if  npercent < 1.00:
                id_label_valid_filename = os.path.join(dirname,\
                    "nidlabel_%i_valid.txt"%(snapID))

        id_label_file = open(id_label_filename, "w")

        id_label_valid_file = ""
        if npercent < 1.00:
           id_label_valid_file = open(id_label_valid_filename, "w")
        dataG = nx.Graph()
        

        #Sample positive edges
        nedges = len(g.edges())
        Logger.logr.info ("Generating %i positive edges "%nedges)  

        shuffled_list = np.arange(nedges)
        np.random.shuffle(shuffled_list) 

        edgelist = g.edges()


        ntrainids = int(len(shuffled_list) * npercent)      


        if  nedges == len(g.edges()):
            for pos in range (0, ntrainids):
                id_label_file.write("%s,%s,pos"\
                        %(edgelist[shuffled_list[pos]][0],\
                            edgelist[shuffled_list[pos]][1]))
                id_label_file.write(os.linesep)
                dataG.add_edge(edgelist[shuffled_list[pos]][0],\
                            edgelist[shuffled_list[pos]][1])
            if  npercent < 1.00:
                for pos in range(ntrainids, len(shuffled_list)):
                    id_label_valid_file.write("%s,%s,pos"%\
                            (edgelist[shuffled_list[pos]][0],\
                                edgelist[shuffled_list[pos]][1]))
                    id_label_valid_file.write(os.linesep)
                    dataG.add_edge(edgelist[shuffled_list[pos]][0],\
                                edgelist[shuffled_list[pos]][1])
    
      
        nodelist = g.nodes()
        train_ids = 0


        while True:
            # Select a node uniformly at random 
            n1 = np.random.choice(len(g.nodes()), 1, replace=True)[0]
            # Select another node uniformly at random
            n2 = np.random.choice(len(g.nodes()), 1, replace=True)[0]
            # Check if the pair exists already, if not dump it into the file
        
            n1 = nodelist [n1] 
            n2 = nodelist [n2]
            
            if  not(g.has_edge(n1, n2)) and n1!=n2 and not(dataG.has_edge(n1, n2)):
                if  train_ids < ntrainids:
                    id_label_file.write("%s,%s,neg"%(n1,n2))
                    id_label_file.write(os.linesep)
                    train_ids = train_ids + 1
                    # Logger.logr.info(train_ids)
                else:
                    if  npercent < 1.00:
                        id_label_valid_file.write("%s,%s,neg"%(n1,n2))
                        id_label_valid_file.write(os.linesep)

                dataG.add_edge (n1, n2)
                
            if  len(dataG.edges()) == 2*nedges:
                break

