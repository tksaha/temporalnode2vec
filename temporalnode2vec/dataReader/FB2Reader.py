#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys 
import numpy
import random 
import datetime
import pandas as pd 
import networkx as nx 
from logManager.Logger import Logger 
from dataReader.DataReader import DataReader




class FB2Reader(DataReader):
    def __init__(self, *args, **kwargs):
        self.fileName = os.environ["FB2FILE"]
        self.startTime = int(os.environ['FB2STTIME'])
        self.endTime = int(os.environ["FB2ENDTIME"])
        os.environ['DATASET'] = 'fb2_%s'%os.environ['TASK']
        self.dataFile = None 
        self.dataDir = os.environ['DATADIR']
        self.timeSlotsToSplit = (self.endTime  - self.startTime + 1 )
        os.environ['NSLOTS'] = str(self.timeSlotsToSplit)
        os.environ['STSNAPID'] = str(0)
        os.environ['ENDSNAPID'] = str(self.endTime)
        self.latSpaceSize = 0

    def processData(self, pd):
        if  pd == 1:
            Logger.logr.info("Starting Processing Data")
            self._processDataMatrixForm()

    def runSmoothedEmbeddingGenerator(self, latSpaceSize):
        self._runEmbeddingGenerator(latSpaceSize)
        
    def runEvaluation(self):
        pass

    