#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys 
import numpy
import random 
import datetime
import pandas as pd 
import networkx as nx 
from logManager.Logger import Logger 
from dataReader.DataReader import DataReader




class NIPSReader(DataReader):
    def __init__(self, *args, **kwargs):
        self.fileName = os.environ["NIPSFILE"]
        self.startTime = int(os.environ['NIPSSTTIME'])
        self.endTime = int(os.environ["NIPSENDTIME"])
        os.environ['DATASET'] = 'nips_%s'%os.environ['TASK']
        self.dataFile = None 
        self.dataDir = os.environ['DATADIR']
        self.timeSlotsToSplit = (self.endTime  - self.startTime + 1 )
        os.environ['NSLOTS'] = str(self.timeSlotsToSplit)
        os.environ['STSNAPID'] = str(13)
        os.environ['ENDSNAPID'] = str(self.endTime)
        self.latSpaceSize = 0

    def processData(self, pd):
        if  pd == 1:
            Logger.logr.info("Starting Processing Data")
            self._processDataMatrixForm()

    def runSmoothedEmbeddingGenerator(self, latSpaceSize):
        self._runEmbeddingGenerator(latSpaceSize)
        
    def runEvaluation(self):
        pass
    