#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys 
import datetime
import pandas as pd
from logManager.Logger import Logger 
from dataReader.DataReader import DataReader
from baselineRunner.DeepWalkRunner import DeepWalkRunner
from baselineRunner.LatentIterUpdateRunner import LatentIterUpdateRunner
from evaluation.classificationevaluaiton.ClassificationEvaluation import ClassificationEvaluation
from baselineRunner.HomogeneousLinearProjectionRunner import HomogeneousLinearProjectionRunner
from baselineRunner.HeterogeneousLinearProjectionRunner import HeterogeneousLinearProjectionRunner


class DBLPReader(DataReader):
    def __init__(self, *args, **kwargs):
        self.fileName = os.environ['DBLPFILE']
        os.environ['DATASET'] = 'dblp'
        self.dataFile = None 
        self.dataDir = os.environ['DATADIR']
        self.timeSlotsToSplit = 11
        self.dateIndex = 3
        self.latSpaceSize = 0

    def processData(self):
        """
        This data has been recorded in only one day. 
        Number of snapshots are based on user choice. 
        """
        Logger.logr.info("Starting Processing DBLP Data")
        self.dataFile = pd.read_csv(self.fileName, sep="\s+", header=None)
        date_converter = lambda x:  pd.to_datetime(datetime.datetime.\
                utcfromtimestamp(x).strftime('%Y-%m-%dT%H:%M:%S'))
        self.dataFile[self.dateIndex] = self.dataFile[self.dateIndex].apply(date_converter)
        uTime = len(list(self.dataFile[self.dateIndex].unique()))
        Logger.logr.info("Found %i unique date"%uTime)


    def runSmoothedEmbeddingGenerator(self, latSpaceSize):

        all_nodes = []
        all_nodes.extend (self.dataFile[0])
        all_nodes.extend (self.dataFile[1])
        all_nodes = set(all_nodes)
        Logger.logr.info("Number of nodes = %i" %len(all_nodes))

        latentSpaceSize = latSpaceSize
        self.latSpaceSize = latSpaceSize


        snapID = 1
        
        v2vec_deep, v2vec_pca, v2vec_spectral, v2vec_mod, v2vec_lle =  {}, {}, {}, {}, {}
        v2vec_mds, v2vec_isomap, v2vec_n2v = {}, {}, {}
        v2vec_line, v2vec_tsvd = {}, {}

        for g in self.getGraphsAtTimes(self.dataFile,\
                 self.dateIndex, 0, 1, self.timeSlotsToSplit):
            for node in all_nodes:
                if  not(g.has_node(node)):
                    g.add_node(node)

            Logger.logr.info ("Total number of nodes in %i th snapshot is %i"%(snapID, len(g.nodes())))
            Logger.logr.info ("Total number of edges in %i th snapshot is %i"%(snapID, len(g.edges())))
    
            # Run various combination to test......

            # Run deepwalk + iterupdate combination 
            
            iterrunner = LatentIterUpdateRunner(method='deepwalk')
            v2vec_deep = iterrunner.runTheBaseline(g, snapID, latSpaceSize, v2vec_deep)

            iterrunner = LatentIterUpdateRunner(method='node2vec')
            v2vec_n2v = iterrunner.runTheBaseline(g, snapID, latSpaceSize, v2vec_n2v)

            iterrunner = LatentIterUpdateRunner(method='line')
            v2vec_line = iterrunner.runTheBaseline(g, snapID, latSpaceSize, v2vec_line)

            # iterrunner = LatentIterUpdateRunner(method='pca')
            # v2vec_pca = iterrunner.runTheBaseline(g, snapID, latSpaceSize, v2vec_pca)

            #iterrunner = LatentIterUpdateRunner(method='tsvd')
            #v2vec_tsvd = iterrunner.runTheBaseline(g, snapID, latSpaceSize, v2vec_tsvd)

            #iterrunner = LatentIterUpdateRunner(method='spectral')
            #v2vec_spectral = iterrunner.runTheBaseline(g, snapID, latSpaceSize, v2vec_spectral)

            #iterrunner = LatentIterUpdateRunner(method='modularity')
            #v2vec_mod = iterrunner.runTheBaseline(g, snapID, latSpaceSize, v2vec_mod)

            #iterrunner = LatentIterUpdateRunner(method='lle')
            #v2vec_lle = iterrunner.runTheBaseline(g, snapID, latSpaceSize, v2vec_lle)

            # iterrunner = LatentIterUpdateRunner(method='mds')
            # v2vec_mds = iterrunner.runTheBaseline(g, snapID, latSpaceSize, v2vec_mds)

            # iterrunner = LatentIterUpdateRunner(method='isomap')
            # v2vec_isomap = iterrunner.runTheBaseline(g, snapID, latSpaceSize, v2vec_isomap)


            if  snapID <= self.timeSlotsToSplit -1:
                # deeprunner = DeepWalkRunner ()
                # deeprunner.reprName = "homogeneous_linear_projection"
                # deeprunner.prepareData(g, snapID)
                # deeprunner.runTheBaseline(latSpaceSize)

                # deeprunner = DeepWalkRunner ()
                # deeprunner.reprName = "heterogeneous_linear_projection"
                # deeprunner.prepareData(g, snapID)
                # deeprunner.runTheBaseline(latSpaceSize)


                deeprunner = DeepWalkRunner ()
                deeprunner.prepareData(g, snapID)
                deeprunner.runTheBaseline(latSpaceSize)

            # Running x + y combination
            snapID = snapID + 1 


        mergedGraph = self.getMergedGraph ()
        for node in all_nodes:
            if  not(mergedGraph.has_node(node)):
                mergedGraph.add_node(node)
                
        deeprunner = DeepWalkRunner ()
        deeprunner.reprName = "deepwalk_for_merged"
        deeprunner.prepareData(mergedGraph, self.timeSlotsToSplit-1)
        deeprunner.runTheBaseline(latSpaceSize)

       # hprojrun = HomogeneousLinearProjectionRunner()
       # hprojrun.prepareData(self.timeSlotsToSplit, all_nodes)
       # hprojrun.runTheBaseline(latentSpaceSize)

       # hetprojrun = HeterogeneousLinearProjectionRunner()
       # hetprojrun.prepareData(self.timeSlotsToSplit, all_nodes)
       # hetprojrun.runTheBaseline(latentSpaceSize)


    def runEvaluation(self):
        composition_method_list = [] 
        method_list = []
        isMerged = False
        self.constructEdgeIDList(isMerged)


        snapID = self.timeSlotsToSplit 
        composition_method = 'hadamard'
        composition_method_list.append (composition_method)



        classeval = ClassificationEvaluation(isMerged=False)
        iterrunner = LatentIterUpdateRunner(method='deepwalk') 
        classeval.generateData(snapID, iterrunner.reprName , composition_method)
        classeval.runClassificationTask(snapID, iterrunner.reprName)
        method_list.append(iterrunner.reprName)


        iterrunner = LatentIterUpdateRunner(method='node2vec')
        classeval.generateData(snapID, iterrunner.reprName , composition_method)
        classeval.runClassificationTask(snapID, iterrunner.reprName)
        method_list.append(iterrunner.reprName)



        iterrunner = LatentIterUpdateRunner(method='line')
        classeval.generateData(snapID, iterrunner.reprName , composition_method)
        classeval.runClassificationTask(snapID, iterrunner.reprName)
        method_list.append(iterrunner.reprName)

        deeprunner = DeepWalkRunner()
        classeval.generateData(snapID, deeprunner.reprName , composition_method)
        classeval.runClassificationTask(snapID, deeprunner.reprName)
        method_list.append(deeprunner.reprName)


        classeval = ClassificationEvaluation(isMerged=True)
        deeprunner = DeepWalkRunner ()
        deeprunner.reprName = "deepwalk_for_merged"
        classeval.generateData(snapID, deeprunner.reprName , composition_method)
        classeval.runClassificationTask(snapID, deeprunner.reprName)
        method_list.append(deeprunner.reprName)
        self.combineResults(composition_method_list, method_list, self.latSpaceSize, snapID)