#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys 
import numpy
import random 
import datetime
import pandas as pandas
import networkx as nx 
from logManager.Logger import Logger 
from dataReader.DataReader import DataReader



class InfectionReader(DataReader):
    def __init__(self, *args, **kwargs):
        self.fileName = os.environ['INFECTFILE']
        os.environ['DATASET'] = 'infect_%s'%os.environ['TASK']
        self.dataFile = None 
        self.dataDir = os.environ['DATADIR']
        os.environ['STSNAPID'] = str(0)
        os.environ['ENDSNAPID'] = str(7)
        self.timeSlotsToSplit = 8 
        os.environ['NSLOTS'] = str(self.timeSlotsToSplit)
        
        self.dateIndex = 3
        self.latSpaceSize = 0

    def processData(self, pd):
        """
        This data has been recorded in only one day. 
        Number of snapshots are based on user choice. 
        """
        self.dataFile = pandas.read_csv(self.fileName, sep="\s+", header=None)
        date_converter = lambda x:  pandas.to_datetime(datetime.datetime.\
                utcfromtimestamp(x).strftime('%Y-%m-%dT%H:%M:%S'))
        self.dataFile[self.dateIndex] = self.dataFile[self.dateIndex].apply(date_converter)

        all_nodes = []
        all_nodes.extend (self.dataFile[0])
        all_nodes.extend (self.dataFile[1])
        all_nodes = sorted(list(set(all_nodes)))

        dict_= {}

        pos = 0
        for nodes in all_nodes:
            dict_[nodes] = pos 
            pos = pos + 1

        index_converter = lambda x: dict_[x]
        self.dataFile[0] = self.dataFile[0].apply(index_converter)
        self.dataFile[1] = self.dataFile[1].apply(index_converter)


        uTime = len(list(self.dataFile[self.dateIndex].unique()))
        Logger.logr.info("Found %i unique date"%uTime)


        if  pd == 1:
            Logger.logr.info("Starting Processing DBLP Data")
            self.getGraphsAtTimes(self.dataFile,\
                 self.dateIndex, 0, 1, self.timeSlotsToSplit)
        

    def runSmoothedEmbeddingGenerator(self, latSpaceSize):
        self._runEmbeddingGenerator(latSpaceSize)
        

    def runEvaluation(self, evalDict):
        pass      

        