import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as manimation


# we generate some synthetic data.
# we will make a list of X, Y lists.
frames = []
frameNumber = 240 # we generate 240 different frames
 
for i in range(0,frameNumber):
    xData = np.linspace(0,2*np.pi)
    yData = np.sin(xData - i * 2.0*np.pi / frameNumber )
    frames.append( [xData, yData] )
    print (xData)
    print (yData)

# now we put them together to make a movie! let's set up the movie writer
framerate = 60 # 24 frames per second
FFMpegWriter = manimation.writers['avconv']
metadata = dict(title='Wave Data', artist='Isaac Newton', comment='')
writer = FFMpegWriter(fps=framerate, metadata=metadata)

# figure setup
fig = plt.figure()
firstFrameX, firstFrameY = frames[0]
l, = plt.plot(firstFrameX, firstFrameY, '-')
 
plt.ylabel( r'$x$ axis' )
plt.xlabel( r'$y$ axis' )
plt.xlim(0,2*np.pi)
plt.title( r'$\lambda = 2 \pi$' )


# let's write to the file!
with writer.saving(fig, "anim.mp4", 100):
    for i in range(frameNumber):
        x, y = frames[i]
        l.set_data( x, y)
        writer.grab_frame()