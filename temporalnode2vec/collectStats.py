import os
import sys
import numpy as np
import networkx as nx 
from utility.ArgumentParserUtility  import ArgumentParserUtility


argparser = ArgumentParserUtility("Process Results")
argparser.addArgumentToParser("dataset", "Please enter dataset "\
        "to work on [enron, smsa, college, infection, fb,"\
        " hepph, dblp, dblp2, dblp3, nips, ytube, fb2 ]", True)
argparser.addArgumentToParser("nsnap", "Please enter latent space size",True)




argparser.parseArgument()

dataset = argparser.getValueOfAnArgument("dataset")
nsnap = int(argparser.getValueOfAnArgument("nsnap"))


#os.environ['DATASET'] = '%s_%s'%(dataset,os.environ['TASK'])
os.environ['DATASET'] = '%smsg'%(dataset)
#os.environ['DATASET'] = '%s'%(dataset)
dataDir = os.environ['DATADIR']






global_graph = nx.Graph()

volume = 0


for i in range (0, nsnap):

    graph_file = os.path.join(dataDir, os.environ['DATASET'],\
                 "graph_%i"\
                 %(i))
    print (graph_file)
    g = nx.read_gpickle(graph_file)
    volume = volume + len(g.edges())

    global_graph.add_edges_from(g.edges())
    global_graph.add_nodes_from(g.nodes())

    print (volume)
    print (len(g.edges()))
    print (g.edges())
    print (len(global_graph.edges()))


print ("nnodes =%i"%len(global_graph.nodes()))
print ("nedges =%i"%len(global_graph.edges()))
print ("volume =%i"%volume)  