#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os 
import sys 
import importlib
from logManager.Logger import Logger 
from utility.ArgumentParserUtility  import ArgumentParserUtility



module_dict = {"enron": "dataReader.EnronReader",\
         "smsa": "dataReader.SMSAReader",\
        "college": "dataReader.CollegeMsgReader",\
        "emaileu": "dataReader.EmailEuReader",\
        "infection": "dataReader.InfectionReader", 
        "fb": "dataReader.FacebookReader",\
        "hepph": "dataReader.HepPHReader",\
        "dblp": "dataReader.DBLPReader",\
        "dblp2": "dataReader.DBLP2Reader",\
        "dblp3": "dataReader.DBLP3Reader",\
        "nips": "dataReader.NIPSReader",\
        "fb2":"dataReader.FB2Reader",\
        "ytube":"dataReader.YouTubeReader" }
class_dict = {"enron":"EnronReader", "smsa": "SMSAReader",\
     "college":"CollegeMsgReader", "emaileu": "EmailEuReader",\
        "infection": "InfectionReader",\
        "fb": "FacebookReader",\
        "hepph": "HepPHReader",\
        "dblp": "DBLPReader",\
        "dblp2": "DBLP2Reader",\
        "dblp3": "DBLP3Reader",\
        "nips": "NIPSReader",\
        "fb2":"FB2Reader",\
        "ytube":"YouTubeReader"} 


def main():
    """
    """
    argparser = ArgumentParserUtility("Temporal Node 2 Vec")
    argparser.addArgumentToParser("dataset", "Please enter dataset "\
        "to work on [enron, smsa, college, infection, fb,"\
        " hepph, dblp, dblp2, dblp3, nips, ytube, fb2 ]", True)
    argparser.parseArgument()

    dataset = argparser.getValueOfAnArgument("dataset")
    module = None 

    try:
        module = module_dict[dataset]
        klass = class_dict[dataset]
    except:
        Logger.logr.error("Dataset Name not found")
        sys.exit() 

    list_of_latent = [32, 64, 128]
    for latSpaceSize in list_of_latent:
        for i in range (1, 11):
            os.environ['RND_SEED'] = str(1000 * i)
                
            Klass = getattr(importlib.import_module(module), klass)
            reader = Klass() 

            Logger.logr.info("Successfully loaded the class")

            if  latSpaceSize== list_of_latent[0]:
                reader.processData(1) # can be changed
            #latSpaceSize = latSpaceSize
            reader.runSmoothedEmbeddingGenerator(latSpaceSize) # can be changed

      
if __name__=="__main__":
    sys.exit(main())