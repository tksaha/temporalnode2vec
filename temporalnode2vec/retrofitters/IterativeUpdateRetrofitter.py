#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os 
import sys 
import networkx as nx 
from copy import deepcopy
from logManager.Logger import Logger

class IterativeUpdateRetrofitter:
    def __init__(self, *args, **kwargs):
      self.numIters = kwargs['numIter']
      self.nx_Graph = kwargs['nx_Graph']
      self.paramDict = kwargs['pdict']

      self.alpha_v = 1.0
      try:
        self.alpha_v = float(self.paramDict['alpha_v'])
      except:
        pass 

    def retrofitWithIterUpdate(self, sen2vec):
      newSen2Vecs = deepcopy(sen2vec)
      allSentenceIds = set(newSen2Vecs.keys())

      for iter_ in range(self.numIters):
        for sentenceId in allSentenceIds:
          sentNeighbors = self.nx_Graph.neighbors(sentenceId)
          numNeighbors = len(sentNeighbors)
          if numNeighbors == 0:
            continue
          newVec = self.alpha_v * numNeighbors * sen2vec[sentenceId]
          for neighborSentId in sentNeighbors:
            newVec += newSen2Vecs[neighborSentId]

          newSen2Vecs[sentenceId] = newVec/(self.alpha_v * numNeighbors  +  numNeighbors)
      return newSen2Vecs
