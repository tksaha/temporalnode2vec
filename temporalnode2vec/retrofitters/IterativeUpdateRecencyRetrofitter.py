#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os 
import sys 
import networkx as nx 
from copy import deepcopy
from sklearn import preprocessing
from numpy import linalg as LA
from logManager.Logger import Logger

class IterativeUpdateRecencyRetrofitter:
    def __init__(self, *args, **kwargs):
      self.numIters = kwargs['numIter']
      self.nx_Graph = kwargs['nx_Graph']
      self.paramDict = kwargs['pdict']

      self.alpha_v = 1.0
      try:
        self.alpha_v = float(self.paramDict['alpha_v'])
      except:
        pass 

      self.percent_neighbor = 1.0
      try:
        self.percent_neighbor = float(self.paramDict['p_n'])
      except:
        pass

    def retrofitWithIterUpdate(self, sen2vec):
        newSen2Vecs = deepcopy(sen2vec)
        allSentenceIds = set(newSen2Vecs.keys())

        for iter_ in range(self.numIters):
          for sentenceId in allSentenceIds:
              sentNeighbors = self.nx_Graph.neighbors(sentenceId)
              numNeighbors = len(sentNeighbors)
              if  numNeighbors == 0:
                  continue

              newVec = self.alpha_v * sen2vec[sentenceId]

              neighbor_add = newSen2Vecs[sentNeighbors[0]]
              for pos in range(1, len(sentNeighbors)):
                  neighbor_add += newSen2Vecs[sentNeighbors[pos]]

              # for neighborSentId in sentNeighbors:
              #   newVec +=   newSen2Vecs[neighborSentId]
          
              newVec = newVec + (neighbor_add)
              newSen2Vecs[sentenceId] = newVec
              newSen2Vecs[sentenceId] = newSen2Vecs[sentenceId] / LA.norm (newSen2Vecs[sentenceId])

        return newSen2Vecs
