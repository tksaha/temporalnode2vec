# Temporal Node 2 Vec Embedding 
In a dynamic network, the neighborhood of the vertices evolve across
different temporal snapshots of the network. Accurate modeling of this temporal evolution 
can help solve complex tasks involving real-life social and interaction networks. 
However, existing models for learning latent representation  are inadequate
for obtaining the representation vectors of the vertices for different time-stamps of
a dynamic network in a meaningful way. In this paper, we propose latent representation
learning models for dynamic networks which overcome the above limitation by considering two different kinds of temporal smoothness: (1) retrofitted, 
and (2) linear transformation. The retrofitted model tracks the representation vector
of a vertex over time, facilitating vertex-based temporal analysis of a network. On the other hand, 
linear transformation based model provides a smooth transition operator which maps 
the representation vectors of all vertices from one temporal snapshot to the next (unobserved)
snapshot---this facilitates prediction of the state of a network in a future time-stamp. We validate 
the performance of our proposed models by employing them for solving the temporal link prediction task.
Experiments on 9 real-life networks from various domains validate that the proposed models are significantly better than the existing models for predicting the dynamics 
of an evolving network.


# If you are using the work, please consider citing the following work:

```
@article{Saha.Hasan:17*4,
  title={Models for Capturing Temporal Smoothness in Evolving Networks for Learning Latent Representation of Nodes},
  author={Tanay Kumar Saha and Thomas Williams and Mohammad Al Hasan and Shafiq Joty and Nicolas K. Varberg},
  journal ={arXiv preprint arXiv:1804.05816},
  year={2018}
}

```


# Requirements
* [Anaconda with Python 3.5](https://www.continuum.io/downloads)


# Python Environment setup and Update
To create the environment, please run the following command:

```
conda env create -f  temporalnode2vec_environment.yml
```

To update environment, please run the following command:
```
conda env update -f temporalnode2vec_environment.yml
```

Now, you have successfully installed temporalnode2vec environment and now you can activate the environment using the following command. 

```
source activate temporalnode2vec_environment
```


If you have added more packages into the environment, you 
can update the .yml file using the following command: 

```
conda env export > temporalnode2vec_environment.yml
```

## Running Deepwalk and Node2Vec algorithm 
Run the following command under the word2vec folder.
```
make clean 
make
chmod +x word2vec
```



## Running BCGD Algorithm for temporal node embedding
Run the following command under the bcgd folder.

```
make clean
make
chmod +x BCGDEmbed
```

## Running Line Algorithm for node embedding
You may need to install gsl package using following command 
(https://askubuntu.com/questions/623339/altough-i-installed-gsl-library-g-cannot-compile-my-code):

```
sudo apt-get install libgsl0-dev
```

## Installing TensorFlow

TensorFlow can be installed using following commands:

```
conda install -c conda-forge tensorflow
export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/gpu/tensorflow_gpu-0.12.1-cp35-cp35m-linux_x86_64.whl 
pip3 install --ignore-installed --upgrade $TF_BINARY_URL
```


if you are having intel MKL error please see 
the following [thread](https://stackoverflow.com/questions/36659453/intel-mkl-fatal-error-cannot-load-libmkl-avx2-so-or-libmkl-def-so).





## Running the Project 
Run vecrepr with -h argument to see all possible options:

```
usage: 

python temporalnode2vec [-h] -dataset DATASET

Temporal Node 2 Vec

optional arguments:
  -h, --help            show this help message and exit
  -dataset DATASET, --dataset DATASET
                        Please enter dataset to work on [enron, smsa, college,
                        infection, fb, hepph, dblp, dblp2, dblp3, nips, ytube,
                        fb2 ]


```


# Processing the Result

```
processResults.py process all the generated results from different random runs.
```



The following command shows the option for running the script.

```
tksaha@tksaha-XPS-13-9350:~/Documents/temporalnode2vec$ python temporalnode2vec/processResults.py -h
usage: processResults.py [-h] -dataset DATASET -lats LATS -rrun RRUN

Process Results

optional arguments:
  -h, --help            show this help message and exit
  -dataset DATASET, --dataset DATASET
                        Please enter dataset to work on [enron, smsa, college,
                        infection, fb, hepph, dblp, dblp2, dblp3, nips, ytube,
                        fb2 ]
  -lats LATS, --lats LATS
                        Please enter latent space size
  -rrun RRUN, --rrun RRUN
                        Please enter the number of random run
                        
```